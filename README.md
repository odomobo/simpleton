      _____ _                 _      _              
     / ____(_)               | |    | |             
    | (___  _ _ __ ___  _ __ | | ___| |_ ___  _ __  
     \___ \| | '_ ` _ \| '_ \| |/ _ \ __/ _ \| '_ \ 
     ____) | | | | | | | |_) | |  __/ || (_) | | | |
    |_____/|_|_| |_| |_| .__/|_|\___|\__\___/|_| |_|
                       | |                          
                       |_|                          


The simpleton chess engine (simpleton) is a simple xboard/winboard-compatible
chess engine, under the MIT license.

## Goals ##
The primary goal of simpleton is to be an alternative reference
engine to [TSCP], with a simple & well-documented codebase for ease of
understanding how a chess engine works.

[TSCP]: http://www.tckerrigan.com/Chess/TSCP/

The secondary goal is to have testability and provability of all
major components, including testing performance. This will be achieved
(in part) by having a testing suite which can evaluate the engine's performance
and strength.

Testability is important because it gives the programmer freedom
to make changes, with the assurance that her changes haven't broken
anything.

The tertiary goal of simpleton is to compile on all major
systems, under all major compilers.

## Design Philosophies ##
The motivating factor in any chess engine is its performance.
To keep things simple, I will only optimize functions based on profiling
results. Although I could implement advanced search algorithms
(like PVS or MTD-f), I'll only use alpha-beta pruning, so as to keep
the codebase simple.

No memory should be dynamically allocated during search, and cache misses
should be kept to a minimum during static evaluation.

Another design philosophy in simpleton is to have simple
data structures for storing information (notably the 0x88 board
layout and square parity for moves).

## How it Works ##
TODO

## Where to Start ##
TODO