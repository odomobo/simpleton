// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once

void generate_moves(bool strict, bool quiesce);
int perft(int depth, bool fast);
void perft_repl(char const * depth_str);
void divide(int depth);
void divide_repl(char const * depth_str);