// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include "types.h"
#include "optimizations.h"

// constants for pieces
const Piece_Type SPACE = 0x0;
const Piece_Type PAWN = 0x1;
const Piece_Type KNIGHT = 0x2;
const Piece_Type BISHOP = 0x3;
const Piece_Type ROOK = 0x4;
const Piece_Type QUEEN = 0x5;
const Piece_Type KING = 0x6;
const Piece_Type PIECE_MASK = 0x7;

inline bool piece_type_can_move_like(Piece_Type piece_type, Offset offset) {
    return optimizations_piece_can_move_like_table[piece_type][offset & 0xFF];
}

// constants for colors
const Color WHITE = 0x0;
const Color BLACK = 0x8;
const Color COLOR_MASK = 0x8;

inline Color color_other(Color color) {
    return color ^ COLOR_MASK;
}

inline int color_to_index(Color color) {
    return color >> 3;
}

inline Color color_from_index(int index) {
    return index << 3;
}

inline bool color_is_white(Color color) {
    return color == WHITE;
}

inline int color_pawn_direction(Color color) {
    return (color == WHITE) ? 1 : -1;
}

inline int color_other_pawn_direction(Color color) {
    return color_pawn_direction(color_other(color));
}

// information for how to display and move a particular piece
struct Piece_Info {
    char const  display[3];
    int         move_direction_count;
    Offset      move_directions[8];
    bool        is_sliding;
};

extern const struct Piece_Info piece_table[7];

Piece piece_from_display(char display);

inline Piece_Type piece_get_type(Piece piece) {
    return piece & PIECE_MASK;
}

inline Color piece_get_color(Piece piece) {
    return piece & COLOR_MASK;
}

inline int piece_get_color_index(Piece piece) {
    return color_to_index(piece_get_color(piece));
}

// unused?
inline char piece_to_display(Piece piece) {
    assert(piece >= SPACE && piece <= (KING|BLACK));
    return piece_table[piece_get_type(piece)].display[piece_get_color_index(piece)];
}

// piece cannot have color information
inline bool piece_can_move_like(Piece piece, Coord offset) {
    return piece_type_can_move_like(piece_get_type(piece), offset);
}

inline int piece_pawn_direction(Piece piece) {
    return color_pawn_direction(piece_get_color(piece));
}

inline Piece piece_get(int rank, int file) {
    return c.board[16*rank + file];
}

inline Piece piece_get_by_coord(Coord co) {
    return c.board[co];
}

inline void piece_set(int rank, int file, Piece p) {
    c.board[16*rank + file] = p;
}