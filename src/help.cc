#include "defs.h"

// private
#define _quote_text(...) #__VA_ARGS__
static char const * const _help_text =
#include "help_txt.dat"
;
// end private

void help_repl(char const * arg) {
    printf("%s", _help_text);
}

void version() {
    char const * development_or_release = (CONFIG_VERSION_MINOR % 2 == 0) ? "" : " (Development)";
    printf("%s by %s version %d.%d%s\n",
        CONFIG_PROGRAM_NAME, CONFIG_AUTHOR_NAME,
        CONFIG_VERSION_MAJOR, CONFIG_VERSION_MINOR, development_or_release);
}

void version_repl(char const * arg) {
    version();
}