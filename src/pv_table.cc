#include <string.h>
#include "defs.h"

// private
void _pv_table_get_best_line_helper(char * buffer, bool extended, int depth);

void pv_table_clear() {
    memset(c.pv_table, 0, sizeof(PV_Entry) * CONFIG_PV_TABLE_SIZE);
}

void pv_table_add_move(Move * m, Score sc, int depth) {
    Stack_Frame * s = stack_frame_get();
    int index = s->zh & PV_TABLE_MASK;
    PV_Entry * e = &c.pv_table[index];

    // We don't necessarily want to overwrite existing entries (incl. for other positions)
    // if the entry has a higher depth than we do (i.e. is a more useful entry)
    if (e->zh != 0 && e->depth >= depth)
        return;

    e->zh = s->zh;
    e->depth = (short)depth; // this is pretty safe; depth should never get beyond the range of a char, let alone a short
    e->score = sc;
    e->best_move = move_to_compact_move(m);
}

PV_Entry * pv_table_get_entry() {
    Stack_Frame * s = stack_frame_get();
    int index = s->zh & PV_TABLE_MASK;
    PV_Entry * e = &c.pv_table[index];

    // if it's empty, or if it doesn't match the current position, return null
    if (e->zh == 0 || e->zh != s->zh) {
        return NULL;
    } else {
        return e;
    }
}

void _pv_table_get_best_line_helper(char * buffer, bool extended, int depth) {
    PV_Entry * e = pv_table_get_entry();
    // If we can't find an entry for this position, we're done.
    // Also, if we get into a loop (i.e. the depth isn't what we expect it to be), we need to break
    if (e == NULL || e->depth != depth)
        return;

    // otherwise, add the entry onto the buffer, push the stack, and call ourself recursively
    Move m;

    // we have to generate moves, so we can find the actual move from the compact move
    generate_moves(false, false);
    move_find_from_compact_move(&m, e->best_move);
    char * move_str = move_to_string(&m, extended);
    sprintf(buffer, "%s ", move_str);
    free(move_str);
    context_perform_move_and_push_stack(&m);
    _pv_table_get_best_line_helper(&buffer[strlen(buffer)], extended, depth-1); // update pointer to the end of the buffer
    context_perform_move_and_pop_stack(&m);
}

// memory is to be freed by caller
char * pv_table_get_best_line(bool extended) {
    char * buffer =(char *)malloc(2048); // hopefully big enough; should be long enough for ~ 20 extended moves
    PV_Entry * e = pv_table_get_entry();
    _pv_table_get_best_line_helper(buffer, extended, e->depth);
    return buffer;
}

void test_pv_table() {
    context_init();
    Stack_Frame * s = stack_frame_get();

    pv_table_clear();

    generate_moves(false, false);
    Move * m = &s->moves[0];
    pv_table_add_move(m, 10, 0);
    PV_Entry * e = pv_table_get_entry();
    mu_assert(e != NULL && e->score == 10);

    context_destroy();
}