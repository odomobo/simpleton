#include "defs.h"

Score result_scores[] = {
    0, // RESULT_NONE
    CHECKMATE_VALUE, // RESULT_CHECKMATE
    0, // RESULT_STALEMATE
    0, // RESULT_INSUFFICIENT_MATERIAL
    0, // RESULT_REPETITION
    0, // RESULT_50_MOVE
};

// precondition: moves must have already been generated from this position
// quiesce has to match the quiesce used on the move generation
Result result_check(bool quiesce) {
    Stack_Frame * s = stack_frame_get();
    // if check- or stalemate
    if (s->moves.count <= 0) {
        if (board_is_king_attacked(c.to_move)) {
            // if in quiescence search, being in check with no attacking moves is really bad, so we'll just call it checkmate
            // TODO: might need to change this when quiescence search is changed
            return RESULT_CHECKMATE;
        } else if (!quiesce) {
            // we can't call it a stalemate if we're in quiescence search -- it's kind of expected that there will be no moves
            return RESULT_STALEMATE;
        }
    }
    
    // TODO: insufficient material

    // 3-move repetition.
    // Note that the history table sees any repetition within the current search tree as a 3-fold repetition
    if (s->repetition_count >= 3) {
        return RESULT_REPETITION;
    }

    // other end-game conditions
    // once halfmoves_since_capture reaches 100, it's officially been 50 moves since anything meaningful has happened
    if (s->halfmoves_since_capture >= 100) {
        return RESULT_50_MOVE;
    }

    // otherwise, no result
    return RESULT_NONE;
}