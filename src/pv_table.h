// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include "types.h"

struct PV_Entry {
    Zobrist_Hash zh;
    Score score;
    Compact_Move best_move;
    short depth;
};

const int   PV_TABLE_MASK = CONFIG_PV_TABLE_SIZE - 1;
void        pv_table_clear();
void        pv_table_add_move(Move * m, Score sc, int depth);
PV_Entry *  pv_table_get_entry();
char *      pv_table_get_best_line(bool extended);