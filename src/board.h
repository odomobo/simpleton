// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include "types.h"

struct Predefined_Board {
    char name[16];
    char fen[128];
};

extern Predefined_Board predefined_boards[];

void        board_init();
void        board_init_repl(char const * arg);
void        board_commit_move(Move * m);
void        board_undo();
void        board_display_repl(char const * arg);
char *      board_get_fen();
void        board_get_fen_repl(char const * arg);
void        board_display_moves(bool extended, bool quiescence);
void        board_display_moves_repl(char const * arg);
bool        board_set_by_name(char const * name);
void        board_set_by_name_repl(char const * name);
bool        board_set_from_fen(char const * fen);
void        board_set_from_fen_repl(char const * fen);
bool        board_is_king_attacked(Color color);
Coord       board_find_king(Color color);
bool        board_find_move_from_string(Move * dst, char const * move_str, bool extended);