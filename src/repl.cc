#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "defs.h"

// private
static void _repl_noop(char const * arg);
static void _repl_testing(char const * arg);

typedef void(*_fp)(char const * arg);
struct _lookup_table_entry {
    char name[16];
    _fp fp;
};

static struct _lookup_table_entry _lookup_table[] = {
    {"help",            help_repl},
    {"version",         version_repl},
    {"xboard",          _repl_noop},
    {"protover",        xboard_protover_repl},
    {"accepted",        _repl_noop},
    {"new",             xboard_new_repl},
    {"setboard",        board_set_from_fen_repl},
    {"getfen",          board_get_fen_repl},
    {"force",           xboard_force_repl},
    {"st",              xboard_st_repl},
    {"level",           xboard_level_repl},
    {"time",            xboard_time_repl},
    {"otim",            _repl_noop},
    {"sd",              xboard_sd_repl},
    {"go",              xboard_go_repl},
    {"usermove",        xboard_usermove_repl},
    {"undo",            xboard_undo_repl},
    {"remove",          xboard_remove_repl},
    {"?",               _repl_noop},
    {"ping",            xboard_ping_repl},
    {"quit",            xboard_quit_repl},
    {"post",            xboard_post_repl},
    {"nopost",          xboard_nopost_repl},
    {"display",         board_display_repl},
    {"displaymoves",    board_display_moves_repl},
    {"setname",         board_set_by_name_repl},
    {"perft",           perft_repl},
    {"divide",          divide_repl},
    {"evaluate",        evaluation_evaluate_position_repl},
    {"search",          search_repl},
    {"test",            _repl_testing},
    {"",                _repl_noop}
};

static const size_t _lookup_table_length = sizeof(_lookup_table)/sizeof(_lookup_table[0]);

static _fp _repl_parse_command(char const * command, char const ** arg_out);
// end private

void repl() {
    while (true) {
        fflush(stdout);
        char line[4096] = "";
        fgets(line, sizeof(line), stdin);
        if (feof(stdin)) {
            exit(0);
        }
        // trim \r and \n
        while (strlen(line) > 0 && (line[strlen(line)-1] == '\n' || line[strlen(line)-1] == '\r')) {
            line[strlen(line)-1] = '\0';
        }

        char const * arg;
        _fp fp = _repl_parse_command(line, &arg);
        if (fp) {
            fp(arg);
            continue;
        }
        
        printf("Error (unknown command): %s\n", line);
    }
}

// arg_out must NOT be freed by caller
// arg_out is optional; pass NULL if not desired
static _fp _repl_parse_command(char const * command, char const ** arg_out) {
    for (unsigned int i = 0; i < _lookup_table_length; i++) {
        struct _lookup_table_entry * t = &_lookup_table[i];
        unsigned int size = strlen(t->name);
        if (strlen(command) < size) {
            t++;
            continue;
        }
        if (memcmp(&t->name[0], command, size) == 0) {
            if (command[size] != '\0' && command[size] != ' ') {
                t++;
                continue;
            }
            if (arg_out) {
                *arg_out = &command[size];
                while (*arg_out[0] == ' ')
                    (*arg_out)++;
            }
            return t->fp;
        }
    }
    return NULL;
}

void test_parse_command() {
    context_init();

    char const * arg = "uninit";
    _fp fp;

    fp = _repl_parse_command("error", &arg);
    mu_assert(fp == NULL);
    mu_assert(strcmp(arg, "uninit") == 0);

    fp = _repl_parse_command("displa", &arg);
    mu_assert(fp == NULL);
    mu_assert(strcmp(arg, "uninit") == 0);

    fp = _repl_parse_command("display", NULL);
    mu_assert(fp == board_display_repl);
    mu_assert(strcmp(arg, "uninit") == 0);

    fp = _repl_parse_command("", &arg);
    mu_assert(fp == _repl_noop);
    mu_assert(strcmp(arg, "") == 0);

    fp = _repl_parse_command("display", &arg);
    mu_assert(fp == board_display_repl);
    mu_assert(strcmp(arg, "") == 0);

    fp = _repl_parse_command("display ", &arg);
    mu_assert(fp == board_display_repl);
    mu_assert(strcmp(arg, "") == 0);

    fp = _repl_parse_command("display  123 456", &arg);
    mu_assert(fp == board_display_repl);
    mu_assert(strcmp(arg, "123 456") == 0);
    context_destroy();
}

static void _repl_noop(char const * arg) {
}

static void _repl_testing(char const * arg) {
    //Stack_Frame * s = stack_frame_get();
}