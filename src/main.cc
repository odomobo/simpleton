#include <stdio.h>
#include "defs.h"

int tests_passed; // required for myunit, until it becomes more robust
Context c;

int main(int argc, char * argv[]) {
    interruption_init();
    context_init();
    repl(); // doesn't return
    return 0;
}