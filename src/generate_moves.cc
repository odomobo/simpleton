#include <string.h>
#include "defs.h"

// private
static bool _try_push_move(Scored_Move * m, Color color_);
static bool _add_simple_move(Coord fco, Coord tco, Piece fp, Piece tp, Color color_);
static void _force_add_simple_move(Coord fco, Coord tco, Piece fp, Piece tp, Color color_);
static void _generate_sliding_moves(Coord co, Piece p, Color color_, bool quiesce);
static void _generate_non_sliding_moves(Coord co, Piece p, Color color_, bool quiesce);
static void _generate_pawn_moves_helper(Coord co, Color color_, Piece dst_piece, bool quiesce);
static void _generate_pawn_moves(Coord co, Color color_, bool strict, bool quiesce);
static void _try_castle_kingside(Color  color_);
static void _try_castle_queenside(Color  color_);
// end private

static bool _try_push_move(Scored_Move * m, Color color_) {
    Stack_Frame * s = stack_frame_get();
    move_perform(m);
    bool check = board_is_king_attacked(color_);
    move_perform(m);
    if (check)
        s->moves.remove_last();
    return !check;
}

// helper function for generate_moves
static bool _add_simple_move(Coord fco, Coord tco, Piece fp, Piece tp, Color color_) {
    Stack_Frame * s = stack_frame_get();
    Scored_Move * m = s->moves.alloc_elem();
    (*m)[0].parity = fp;
    (*m)[0].coord = fco;
    (*m)[1].parity = fp ^ tp;
    (*m)[1].coord = tco;
    (*m)[2].parity = 0;
    return _try_push_move(m, color_);
}

// doesn't attempt check king safety
static void _force_add_simple_move(Coord fco, Coord tco, Piece fp, Piece tp, Color  color_) {
    Stack_Frame * s = stack_frame_get();
    Scored_Move * m = s->moves.alloc_elem();
    (*m)[0].parity = fp;
    (*m)[0].coord = fco;
    (*m)[1].parity = fp ^ tp;
    (*m)[1].coord = tco;
    (*m)[2].parity = 0;
}

static void _generate_sliding_moves(Coord co, Piece p, Color  color_, bool quiesce) {
    Stack_Frame * s = stack_frame_get();
    Piece_Type pt = piece_get_type(p);
    for (int i = 0; i < piece_table[pt].move_direction_count; i++) {
        Offset offset = piece_table[pt].move_directions[i];
        for (int j = 1; ; j++) {
            Coord cot = co + (offset * j);
            if (!coord_is_valid(cot))
                break;
            Piece q = piece_get_by_coord(cot);
            Piece_Type qt = piece_get_type(q);
            // if the piece lookup is the same color_ as the current piece, then we can't proceed
            if (qt != SPACE && piece_get_color(q) == color_)
                break;

            // if we're not in check, then if we attempt an invalid move, all the rest of 
            // the moves in this direction will also be invalid
            if (s->in_check || j == 1) {
                bool success = _add_simple_move(co, cot, p, q, color_);
                if (!s->in_check && !success)
                    break;
                // If we made a quiet move, we need to remove it.
                // The main reason we even made a quiet move was to see if we would be placed in check.
                if (quiesce && qt == SPACE && success)
                    s->moves.remove_last();
            } else {
                // don't do a king safety check, because if we succeeded on our first move, all the rest of
                // the moves in this direction will be valid
                if (!quiesce || qt != SPACE)
                    _force_add_simple_move(co, cot, p, q, color_);
            }

            // if it's not an attacking move, keep going
            // otherwise, we're done
            if (q == SPACE) {
                continue;
            } else {
                break;
            }
        }
    }
}

static void _generate_non_sliding_moves(Coord co, Piece p, Color  color_, bool quiesce) {
    Piece_Type pt = piece_get_type(p);
    for (int i = 0; i < piece_table[pt].move_direction_count; i++) {
        Offset offset = piece_table[pt].move_directions[i];
        Coord cot = co + offset;
        if (!coord_is_valid(cot))
            continue;
        Piece q = piece_get_by_coord(cot);
        Piece_Type qt = piece_get_type(q);
        // if the piece lookup is the same color_ as the current piece, then try the next move
        if (qt && piece_get_color(q) == color_)
            continue;

        // if the piece lookup is a space and we're in quiescence search, try the next move
        if (quiesce && qt == SPACE)
            continue;

        _add_simple_move(co, cot, p, q, color_);
    }
}

static void _generate_pawn_moves_helper(Coord co, Color  color_, Piece dst_piece, bool quiesce) {
    Stack_Frame * s = stack_frame_get();
    int home_rank = (color_ == WHITE) ? 0x10 : 0x60;
    int direction = color_pawn_direction(color_);
    Coord cot;

    // try normal move, if not quiescence search
    if (!quiesce) {
        cot = co + coord_get(direction, 0);
        if (coord_is_valid(cot) && piece_get_by_coord(cot) == SPACE) {
            if (piece_get_by_coord(cot) == SPACE) {
                Scored_Move * m = s->moves.alloc_elem();
                (*m)[0].parity = (PAWN|color_);
                (*m)[0].coord = co;
                (*m)[1].parity = dst_piece;
                (*m)[1].coord = cot;
                (*m)[2].parity = 0;
                // tell the move displaying function whether or not this is a promotion, by storing
                // the destination value in an otherwise-unused element
                (*m)[3].parity = dst_piece;
                _try_push_move(m, color_);
            }

            // try double move
            if ((co & RANK_MASK) == home_rank) {
                cot = co + coord_get(direction*2, 0);
                if (piece_get_by_coord(cot) == SPACE) {
                    Scored_Move * m = s->moves.alloc_elem();
                    (*m)[0].parity = (PAWN|color_);
                    (*m)[0].coord = co;
                    (*m)[1].parity = dst_piece;
                    (*m)[1].coord = cot;
                    (*m)[2].parity = 0;
                    // tell the move displaying function whether or not this is a promotion, by storing
                    // the destination value in an otherwise-unused element
                    (*m)[3].parity = dst_piece;
                    _try_push_move(m, color_);
                }
            }
        }
    }
    // try captures
    for (int i = -1; i <= 1; i += 2) {
        cot = co + coord_get(direction, i);
        if (!coord_is_valid(cot))
            continue;
        Piece q = piece_get_by_coord(cot);
        if (piece_get_type(q) && piece_get_color(q) != color_) {
            Scored_Move * m = s->moves.alloc_elem();
            (*m)[0].parity = (PAWN|color_);
            (*m)[0].coord = co;
            (*m)[1].parity = dst_piece ^ q;
            (*m)[1].coord = cot;
            (*m)[2].parity = 0;
            // tell the move displaying function whether or not this is a promotion, by storing
            // the destination value in an otherwise-unused element
            (*m)[3].parity = dst_piece;
            _try_push_move(m, color_);
        } else if (q == SPACE && cot == s->en_passant) { // try en passant capture
            Scored_Move * m = s->moves.alloc_elem();
            (*m)[0].parity = (PAWN|color_);
            (*m)[0].coord = co;
            (*m)[1].parity = (PAWN|color_);
            (*m)[1].coord = cot;
            (*m)[2].parity = (PAWN|color_other(color_));
            (*m)[2].coord = co + coord_get(0, i);
            (*m)[3].parity = 0;
            _try_push_move(m, color_);
        }
    }
}

// helper function for generate_moves
static void _generate_pawn_moves(Coord co, Color  color_, bool strict, bool quiesce) {
    int final_rank;
    if (color_ == WHITE) {
        final_rank = 0x60;
    } else {
        final_rank = 0x10;
    }

    if ((co & RANK_MASK) != final_rank) {
        _generate_pawn_moves_helper(co, color_, (PAWN|color_), quiesce);
    } else {
        _generate_pawn_moves_helper(co, color_, (QUEEN|color_), quiesce);

        // TODO: give an option to remove the following 3 underpromotions
        if (strict) {
            _generate_pawn_moves_helper(co, color_, (KNIGHT|color_), quiesce);
            _generate_pawn_moves_helper(co, color_, (ROOK|color_), quiesce);
            _generate_pawn_moves_helper(co, color_, (BISHOP|color_), quiesce);
        }
    }
}

// assumes that there is still kingside castle availability
static void _try_castle_kingside(Color  color_) {
    Stack_Frame * s = stack_frame_get();
    int rank;
    if (color_ == WHITE) {
        rank = 0;
    } else {
        rank = 7;
    }
    for (int i = 5; i <= 6; i++) {
        if (piece_get(rank, i) != SPACE)
            return;
        if (coord_is_attacked(coord_get(rank, i), color_))
            return;
    }
    if (coord_is_attacked(coord_get(rank, 4), color_))
        return;

    // if none of the above, then we are free to castle kingside
    Scored_Move * m = s->moves.alloc_elem();
    (*m)[0].parity = (KING|color_);
    (*m)[0].coord = coord_get(rank, 4);
    (*m)[1].parity = (KING|color_);
    (*m)[1].coord = coord_get(rank, 6);
    (*m)[2].parity = (ROOK|color_);
    (*m)[2].coord = coord_get(rank, 7);
    (*m)[3].parity = (ROOK|color_);
    (*m)[3].coord = coord_get(rank, 5);
    // we've already ensured the king won't be in check, so no need to perform the check again
}

// assumes that there is still queenside castle availability
static void _try_castle_queenside(Color  color_) {
    Stack_Frame * s = stack_frame_get();
    int rank;
    if (color_ == WHITE) {
        rank = 0;
    } else {
        rank = 7;
    }
    // square b1 or b8 has to be empty, but it doesn't matter if it is attacked
    if (piece_get(rank, 1) != SPACE)
        return;
    for (int i = 2; i <= 3; i++) {
        if (piece_get(rank, i) != SPACE)
            return;
        if (coord_is_attacked(coord_get(rank, i), color_))
            return;
    }
    if (coord_is_attacked(coord_get(rank, 4), color_))
        return;

    // if none of the above, then we are free to castle kingside
    Scored_Move * m = s->moves.alloc_elem();
    (*m)[0].parity = (KING|color_);
    (*m)[0].coord = coord_get(rank, 4);
    (*m)[1].parity = (KING|color_);
    (*m)[1].coord = coord_get(rank, 2);
    (*m)[2].parity = (ROOK|color_);
    (*m)[2].coord = coord_get(rank, 0);
    (*m)[3].parity = (ROOK|color_);
    (*m)[3].coord = coord_get(rank, 3);
    // we've already ensured the king won't be in check, so no need to perform the check again
}

// Move generation.
// Steps through every square of the board. Generates moves if the square contains a piece of the correct color_.
// Ensures that king is safe after each move. This is done by making the move, checking king safety,
// and then undoing the move.
// Strict determines whether or not to generate underpromotions
void generate_moves(bool strict, bool quiesce) {
    Stack_Frame * s = stack_frame_get();
    s->moves.clear();
    Color color_ = c.to_move;

    for (int rank = 0; rank < 8; rank++) {
        for (int file = 0; file < 8; file++) {
            Coord co = coord_get(rank, file);
            Piece p = piece_get_by_coord(co);
            Piece_Type pt = piece_get_type(p);
            // if it's a piece of the right color_
            if (pt && piece_get_color(p) == color_) {
                // first try to make normal moves
                // we have to do different logic depending on whether or not we are sliding
                if (piece_table[pt].is_sliding) {
                    _generate_sliding_moves(co, p, color_, quiesce);
                } else {
                    _generate_non_sliding_moves(co, p, color_, quiesce);
                }
                // if pawn, try pawn moves
                if (pt == PAWN) {
                    _generate_pawn_moves(co, color_, strict, quiesce);
                }
                // if king, try castling
                // skip this step in quiescence search
                if (pt == KING && !quiesce) {
                    if (castling_check_kingside(s, color_)) {
                        _try_castle_kingside(color_);
                    }
                    if (castling_check_queenside(s, color_)) {
                        _try_castle_queenside(color_);
                    }
                }
            }
        }
    }
}

static void _test_generate_moves_quiescence_helper() {
    Stack_Frame * s = stack_frame_get();
    mu_assert(c.stack_depth == 0);

    Scored_Move tmp[1024];
    
    generate_moves(true, true);
    int tmp_count = s->moves.count;
    memcpy(tmp, &s->moves[0], sizeof(Scored_Move) * tmp_count);
    generate_moves(true, false);

    int total_attacks = 0;
    // first loop over normally-generated moves, looking for attacks
    for (int i = 0; i < s->moves.count; i++) {
        // if we found an attack
        if (move_is_capture(&s->moves[i])) {
            total_attacks++;
            bool found_move = false;
            for (int j = 0; j < tmp_count; j++) {
                if (moves_do_match(&s->moves[i], &tmp[j])) {
                    found_move = true;
                    break;
                }
            }
            if (!found_move) {
                char * fen = board_get_fen();
                char * move_str = move_to_string(&s->moves[i], true);
                debug("Couldn't find move %s in fen %s\n", move_str, fen);
                free(move_str);
                free(fen);
                mu_assert(false);
            }
        }
    }
    mu_assert(total_attacks == tmp_count);
}

void test_generate_moves_quiescence() {
    context_init();
    
    for (int i = 0; predefined_boards[i].name[0] != '\0'; i++) {
        board_set_from_fen(predefined_boards[i].fen);
        _test_generate_moves_quiescence_helper();
    }

    context_destroy();
}

int perft(int depth, bool fast) {
    Stack_Frame * s = stack_frame_get();
    if (depth <= 0) {
        return 1;
    } else if (depth == 1 && fast) {
        generate_moves(true, false);
        return s->moves.count;
    }

    int accum = 0;
    generate_moves(true, false);
    for (int i = 0; i < s->moves.count; i++) {
        context_perform_move_and_push_stack(&s->moves[i]);
        accum += perft(depth-1, fast);
        context_perform_move_and_pop_stack(&s->moves[i]);
    }
    return accum;
}

void perft_repl(char const * depth_str) {
    long long t1 = utils_get_time_milliseconds();

    int depth = atoi(depth_str);
    int result = perft(depth, true);
    
    long long t2 = utils_get_time_milliseconds();

    printf("%d\n", result);

    float seconds = (t2 - t1) / 1000.0f;
    if (seconds > 0.005) {
        int nodes_per_second = (int)(result / seconds);
        float million_nodes_per_second = nodes_per_second / 1000000.0f;
        printf("In %.3f seconds, for %.3f million nodes per second\n", seconds, million_nodes_per_second);
    } else {
        printf("In %.3f seconds, for ??? million nodes per second\n", seconds);
    }
}

void divide(int depth) {
    Stack_Frame * s = stack_frame_get();
    generate_moves(true, false);
    for (int i = 0; i < s->moves.count; i++) {
        context_perform_move_and_push_stack(&s->moves[i]);
        char * move_string = move_to_string(&s->moves[i], false);
        int p = perft(depth-1, true);
        printf("%s %d\n", move_string, p);
        free(move_string);
        context_perform_move_and_pop_stack(&s->moves[i]);
    }
}

void divide_repl(char const * depth_str) {
    int depth = atoi(depth_str);
    divide(depth);
}

// Calls perft on a few different boards, and checks against known values.
void test_perft() {
    context_init();
    int init_1 = perft(1, true);
    debug_int(init_1);
    mu_assert(init_1 == 20);
    int init_2 = perft(2, true);
    debug_int(init_2);
    mu_assert(init_2 == 400);
    int init_3 = perft(3, true);
    debug_int(init_3);
    mu_assert(init_3 == 8902);
    int init_4 = perft(4, true);
    debug_int(init_4);
    mu_assert(init_4 == 197281);
    int init_5 = perft(5, true);
    debug_int(init_5);
    mu_assert(init_5 == 4865609);

    board_set_by_name("perft3");
    int perft3_1 = perft(1, true);
    debug_int(perft3_1);
    mu_assert(perft3_1 == 14);
    int perft3_2 = perft(2, true);
    debug_int(perft3_2);
    mu_assert(perft3_2 == 191);
    int perft3_3 = perft(3, true);
    debug_int(perft3_3);
    mu_assert(perft3_3 == 2812);
    int perft3_4 = perft(4, true);
    debug_int(perft3_4);
    mu_assert(perft3_4 == 43238);

    board_set_by_name("perft1");
    int perft1_1 = perft(1, true);
    debug_int(perft1_1);
    mu_assert(perft1_1 == 48);
    int perft1_2 = perft(2, true);
    debug_int(perft1_2);
    mu_assert(perft1_2 == 2039);
    int perft1_3 = perft(3, true);
    debug_int(perft1_3);
    mu_assert(perft1_3 == 97862);
    int perft1_4 = perft(4, true);
    debug_int(perft1_4);
    mu_assert(perft1_4 == 4085603);
    context_destroy();
}
