#include <stdlib.h>
#include <string.h>
#include "defs.h"

void Move_Vector::init(int capacity) {
    _capacity = capacity;
    count = 0;
    _data = (Scored_Move *)malloc(sizeof(Scored_Move) * capacity);
    assert(_data != NULL);
}

void Move_Vector::destroy() {
    free(_data);
}

Scored_Move * Move_Vector::alloc_elem() {
    if (count == _capacity) {
        _data = (Scored_Move *)realloc(_data, sizeof(Scored_Move) * _capacity * 2);
        assert(_data != NULL);
        _capacity *= 2;
    }

    // only increment count after indexing using its old value
    return &_data[count++];
}

// ensures Move_Vector does what it needs to
void test_move_vector() {
    Move_Vector v;
    v.init(2);

    Scored_Move m1; m1[0].parity = (KING|WHITE); m1[0].coord = coord_get(1, 1); m1.score = 10;
    Scored_Move m2; m2[0].parity = (ROOK|WHITE); m2[0].coord = coord_get(2, 2); m2.score = 20;
    Scored_Move m3; m3[0].parity = (PAWN|WHITE); m3[0].coord = coord_get(3, 3); m3.score = 30;

    *v.alloc_elem() = m1;
    mu_assert(v._capacity == 2);
    *v.alloc_elem() = m2;
    mu_assert(v._capacity == 2);
    *v.alloc_elem() = m3;
    mu_assert(v._capacity == 4);
    mu_assert(memcmp(&v[1], &m2, sizeof(Scored_Move)) == 0);
    mu_assert(memcmp(&v[2], &m3, sizeof(Scored_Move)) == 0);
    v.destroy();
}

void Move_Vector::remove_last() {
    assert(count > 0);
    count--;
}

void Move_Vector::sort() {
    // we have to start at 1, otherwise our optimization would do bad things ("0 - 1" is a bad index)
    for (int i = 1; i < count; i++) {
        // optimization to not copy the move unless we need to move at least 1 position
        if (_data[i-1].score > _data[i].score) {
            Scored_Move tmp = _data[i];
            _data[i] = _data[i-1]; // we've already checked that we should do this in the for loop, so let's do it
            int j;
            for (j = i-1; j > 0 && _data[j-1].score > tmp.score; j--) {
                _data[j] = _data[j-1];
            }
            _data[j] = tmp;
        }
    }
}

void test_move_vector_sort() {
    Move_Vector v;
    v.init(10);

    Scored_Move m1; m1[0].parity = 0; m1[0].coord = coord_get(1, 1); m1.score = 10;
    Scored_Move m2; m2[0].parity = 1; m2[0].coord = coord_get(2, 2); m2.score = 20;
    Scored_Move m3; m3[0].parity = 2; m3[0].coord = coord_get(3, 3); m3.score = 30;
    Scored_Move m4; m4[0].parity = 3; m4[0].coord = coord_get(4, 4); m4.score = 40;
    Scored_Move m5; m5[0].parity = 4; m5[0].coord = coord_get(5, 5); m5.score = 50;

    *v.alloc_elem() = m5;
    *v.alloc_elem() = m1;
    *v.alloc_elem() = m3;
    *v.alloc_elem() = m2;
    *v.alloc_elem() = m4;
    
    v.sort();
    for (int i = 0; i < 5; i++) {
        mu_assert(v[i][0].parity == i);
    }

    v.clear();

    *v.alloc_elem() = m1;
    *v.alloc_elem() = m2;
    *v.alloc_elem() = m5;
    *v.alloc_elem() = m3;
    *v.alloc_elem() = m4;

    v.sort();
    for (int i = 0; i < 5; i++) {
        mu_assert(v[i][0].parity == i);
    }

    v.destroy();
}

void Move_Vector::clear() {
    count = 0;
}