#include <math.h>
#include <stdio.h>
#include "defs.h"

const Evaluation_Table_Entry evaluation_table[7] = {
    { // space
        0 // material value
    },
    { // pawn
        1, // material value
        1.8f // average number of moves
    },
    { // knight
        3, // material value
        6 // average number of moves
    },
    { // bishop
        3, // material value
        6 // average number of moves
    },
    { // rook
        5, // material value
        10 // average number of moves
    },
    { // queen
        9, // material value
        18 // average number of moves
    },
    { // king
        5, // material value is used for checks and x-rays
        6.5f // average number of moves
    }
};

// private
static float _square_values[128];

static void _normalize_values_array(float * arr);
static Score _evaluate_material(Piece p);
static void _evaluate_utilization_quiet(Piece p, Coord co, Evaluation_Info * info, float utilization);
static void _evaluate_utilization_defense(Piece p, Piece defended, Coord co, Evaluation_Info * info, float utilization);
static void _evaluate_utilization_attack(Piece p, Piece attacked, Coord co, Evaluation_Info * info, float utilization);
static void _evaluate_sliding_piece(Coord co, Piece p, Evaluation_Info * info);
static void _evaluate_non_sliding_piece(Coord co, Piece p, Evaluation_Info * info);
static void _evaluate_pawn(Coord co, Piece p, Evaluation_Info * info);
// end private

static void _normalize_values_array(float * arr) {
    float accum = 0;
    for (int rank = 0; rank < 8; rank++) {
        for (int file = 0; file < 8; file++) {
            Coord co = coord_get(rank, file);
            accum += arr[co];
        }
    }
    float average = accum / 64.0f;
    for (int rank = 0; rank < 8; rank++) {
        for (int file = 0; file < 8; file++) {
            Coord co = coord_get(rank, file);
            arr[co] /= average;
        }
    }
}

void evaluation_init() {
    // for now, square values just contains a linear increase near the center
    float max_distance = 5.0f;
    for (int rank = 0; rank < 8; rank++) {
        for (int file = 0; file < 8; file++) {
            float rank_distance = 3.5f - rank;
            float rank_distance_sq = rank_distance * rank_distance;
            float file_distance = 3.5f - file;
            float file_distance_sq = file_distance * file_distance;
            Coord co = coord_get(rank, file);
            _square_values[co] = max_distance - sqrtf(rank_distance_sq + file_distance_sq);
        }
    }
    _normalize_values_array(_square_values);
}

static Score _evaluate_material(Piece p) {
    return evaluation_table[piece_get_type(p)].material_value;
}

void test_evaluate_material() {
    context_init();

    Evaluation_Info info;
    board_set_from_fen("4kb2/8/8/8/8/8/P7/4K3 w - - 0 1");
    evaluation_evaluate_position(&info);
    mu_assert(info.material_values[0] == 1);
    mu_assert(info.material_values[1] == 3);

    board_set_from_fen("4k2r/8/8/8/8/8/8/4K1N1 w - - 0 1");
    evaluation_evaluate_position(&info);
    mu_assert(info.material_values[0] == 3);
    mu_assert(info.material_values[1] == 5);

    board_set_from_fen("r3k2r/8/8/8/8/8/8/3QK3 w - - 0 1");
    evaluation_evaluate_position(&info);
    mu_assert(info.material_values[0] == 9);
    mu_assert(info.material_values[1] == 10);

    context_destroy();
}

// Note that co is the coord that the piece is eyeing, not
// the coord the piece is on.
static void _evaluate_utilization_quiet(Piece p, Coord co, Evaluation_Info * info, float utilization) {
    float square_value = _square_values[co];
    if (piece_get_type(p) != KING) {
        info->piece_utilization_values[piece_get_color_index(p)] += utilization * square_value / evaluation_table[piece_get_type(p)].average_number_of_moves;
    } else {
        info->king_utilization_values[piece_get_color_index(p)] += utilization * square_value / evaluation_table[piece_get_type(p)].average_number_of_moves;
    }
    
}
static void _evaluate_utilization_defense(Piece p, Piece defended, Coord co, Evaluation_Info * info, float utilization) {
    _evaluate_utilization_quiet(p, co, info, utilization);
}
static void _evaluate_utilization_attack(Piece p, Piece attacked, Coord co, Evaluation_Info * info, float utilization) {
    _evaluate_utilization_quiet(p, co, info, utilization);
}

static void _evaluate_sliding_piece(Coord co, Piece p, Evaluation_Info * info) {
    for (int i = 0; i < piece_table[piece_get_type(p)].move_direction_count; i++) {
        Offset offset = piece_table[piece_get_type(p)].move_directions[i];
        // the utilization amount will shrink after an x-ray through a minor
        // or major piece which doesn't move in the same direction
        float utilization = 1;
        for (int j = 1; ; j++) {
            Coord cot = co + (offset * j);
            if (!coord_is_valid(cot))
                break;
            Piece q = piece_get_by_coord(cot);
            // if the piece lookup is the same color as the current piece, then we can't proceed
            if (piece_get_type(q) == SPACE) {
                _evaluate_utilization_quiet(p, co, info, utilization);
            } else if (piece_get_color(q) == piece_get_color(p)) {
                _evaluate_utilization_defense(p, q, co, info, utilization);
                // Check for x-rays.
                // Can't x-ray through pawn or king.
                if (piece_get_type(q) == PAWN || piece_get_type(q) == KING)
                    break;
                if (piece_can_move_like(q, offset)) {
                    // utilization_amount stays at 100%, because we're both travelling in the same direction
                } else {
                    // for now, let's halve utilization amount through x-rays
                    utilization *= 0.5f;
                }
            } else {
                _evaluate_utilization_attack(p, q, co, info, utilization);
                // can't keep looking
                break;
            }
        }
    }
}

static void _evaluate_non_sliding_piece(Coord co, Piece p, Evaluation_Info * info) {
    for (int i = 0; i < piece_table[piece_get_type(p)].move_direction_count; i++) {
        Offset offset = piece_table[piece_get_type(p)].move_directions[i];
        Coord cot = co + offset;
        if (!coord_is_valid(cot))
            continue;
        Piece q = piece_get_by_coord(cot);
        if (piece_get_type(q) == SPACE) {
            _evaluate_utilization_quiet(p, co, info, 1);
        } else if (piece_get_color(q) == piece_get_color(p)) {
            _evaluate_utilization_defense(p, q, co, info, 1);
        } else {
            _evaluate_utilization_attack(p, q, co, info, 1);
        }
    }
}

static void _evaluate_pawn(Coord co, Piece p, Evaluation_Info * info) {
    int direction = piece_pawn_direction(p);
    for (int i = -1; i <= 1; i += 2) {
        Coord cot = co + coord_get(direction, i);
        if (!coord_is_valid(cot))
            continue;
        Piece q = piece_get_by_coord(cot);
        if (piece_get_type(q) == SPACE) {
            _evaluate_utilization_quiet(p, co, info, 1);
        } else if (piece_get_color(q) == piece_get_color(p)) {
            _evaluate_utilization_defense(p, q, co, info, 1);
        } else {
            _evaluate_utilization_attack(p, q, co, info, 1);
        }
    }
}

// Works mostly the same as normal generate_moves, except has some changes specific to the data that
// evaluation_evaluate_position is expecting. For example, there's a dummy move added per piece, so evaluation_evaluate_position
// can easily see material. Also, pieces are allowed to x-ray through friendly pieces which move in the same
// way.
Score evaluation_evaluate_position(Evaluation_Info * info_out) {
    Stack_Frame * s = stack_frame_get();
    c.counter++; // only increment the node counter for every static evaluation we perform
    Evaluation_Info info = { 0 };
    for (int rank = 0; rank < 8; rank++) {
        for (int file = 0; file < 8; file++) {
            Coord co = coord_get(rank, file);
            Piece p = piece_get_by_coord(co);
            // if it's a piece of any color
            if (piece_get_type(p) != SPACE) {
                Color color = piece_get_color(p);

                info.material_values[color_to_index(color)] += _evaluate_material(p);

                // then try to make normal moves
                // we have to do different logic depending on whether or not we are sliding
                // TODO: x-rays
                if (piece_table[piece_get_type(p)].is_sliding) {
                    _evaluate_sliding_piece(co, p, &info);
                } else {
                    _evaluate_non_sliding_piece(co, p, &info);
                }
                // if pawn, try pawn moves
                if (piece_get_type(p) == PAWN) {
                    _evaluate_pawn(co, p, &info);
                }
            }
        }
    }
    
    for (int i = 0; i < 2; i++) {
        // correct the material values, so as not to include the kings
        info.material_values[i] -= evaluation_table[KING].material_value;

        // Update king utilization values depending on how much material the opponent has.
        // For now, we assume anything over 12 pawns is too much material.
        int j = 1 - i;
        if (info.material_values[i] > 12) {
            // negative utilization
            info.king_utilization_values[j] *= -1;
            // When the material switches, we want the overall king utilization value to stay near the same.
            // The king is probably at around -0.5 when in the corner and above 12 material.
            // Then king is probably at around 0.1 when in the corner and below 12 material.
            info.king_utilization_values[j] += 0.6f;
            // make sure the king doesn't get a huge boost from being right in the corner
            info.king_utilization_values[j] = fmaxf(info.king_utilization_values[j], 0.33f);
        } else {
            // positive (smaller) utilization
            info.king_utilization_values[j] *= 0.3f;
        }
    }

    // If a side is in check, their piece utilization value is overvalued.
    // Honestly, quiescense search shouldn't be stopping on these anyhow.
    if (s->in_check) {
        info.piece_utilization_values[color_to_index(c.to_move)] *= 0;
    }
    info.white_evaluation = info.material_values[0] - info.material_values[1];
    info.white_evaluation += (info.piece_utilization_values[0] - info.piece_utilization_values[1]) * 0.3f;
    info.white_evaluation += info.king_utilization_values[0] - info.king_utilization_values[1];

    if (info_out != NULL)
        *info_out = info;
    return (c.to_move == WHITE) ? info.white_evaluation : -info.white_evaluation;
}

void evaluation_evaluate_position_repl(char const * arg) {
    Evaluation_Info info;
    float value = evaluation_evaluate_position(&info);
    printf("# %f\n", value);
    printf("# Material: (White: %f; Black: %f)\n", info.material_values[0], info.material_values[1]);
    printf("# Utilization: (White: %f; Black: %f)\n", info.piece_utilization_values[0], info.piece_utilization_values[1]);
    printf("# King Utilization: (White: %f; Black: %f)\n", info.king_utilization_values[0], info.king_utilization_values[1]);
}