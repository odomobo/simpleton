#pragma once
#include "types.h"

const int       HISTORY_TABLE_MASK = CONFIG_HISTORY_TABLE_SIZE - 1;

void            history_table_clear();
void            history_table_add(Stack_Frame * s);
void            history_table_remove(Stack_Frame * rem);
Stack_Frame *   history_table_find(Zobrist_Hash zh);
bool            history_table_verify();