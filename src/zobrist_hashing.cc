#include "defs.h"

// This table is taken from the polyglot book format.
// However, it's a perfectly fine table, so let's use it (although with
// slight incompatibility with the polyglot book format).
const Zobrist_Hash zobrist_hash_keys[781] = {
#include "zobrist_hash_keys.dat"
};

// this is relatively slow; better to normally find a new hash value by making just an incremental
// change from the previous hash value
Zobrist_Hash zobrist_hashing_generate_from_board() {
    Stack_Frame * s = stack_frame_get();
    Zobrist_Hash zh = 0;
    for (int rank = 0; rank < 8; rank++) {
        for (int file = 0; file < 8; file++) {
            Piece p = piece_get(rank, file);
            if (p == SPACE)
                continue;

            zh ^= zobrist_hashing_hash_for_piece(p, rank, file);
        }
    }
    for (int i = 0; i < 4; i++) {
        if (s->can_castle[i])
            zh ^= zobrist_hashing_hash_for_castle(i);
    }
    zh ^= zobrist_hashing_hash_for_en_passant(s->en_passant);
    if (c.to_move == WHITE)
        zh ^= zobrist_hashing_hash_for_white();
    return zh;
}

Zobrist_Hash zobrist_hashing_generate_polyglot_hash_from_board() {
    Stack_Frame * s = stack_frame_get();
    // This does 99% of the work for us; all we have to do is undo
    // the en passant hash if we did an en passant and there's no suitable pawn to take it
    Zobrist_Hash zh = zobrist_hashing_generate_from_board();
    if (s->en_passant == COORD_INVALID)
        return zh;

    bool valid_ep = false;
    int rank_offset = -color_pawn_direction(c.to_move);
    int rank_to_check = rank_offset + (s->en_passant & RANK_MASK)/16;
    for (int i = -1; i <= 1; i += 2) {
        int file_to_check = i + (s->en_passant & FILE_MASK);
        Piece p = piece_get(rank_to_check, file_to_check);
        if (piece_get_type(p) == PAWN && piece_get_color(p) == c.to_move)
            valid_ep = true;
    }
    if (!valid_ep) {
        zh ^= zobrist_hashing_hash_for_en_passant(s->en_passant);
    }
    return zh;
}

// this looks like a funky algorithm, and it is. The zobrist keys are:
// pPnNbBrRqQkK
// but piece uses the format:
// _PNBRQK..._pnbrqk
int zobrist_hashing_piece_to_key(Piece p) {
    return (piece_get_type(p)-1) * 2 + (1-piece_get_color_index(p));
}

Zobrist_Hash zobrist_hashing_hash_for_piece(Piece p, int rank, int file) {
    int zobrist_piece = zobrist_hashing_piece_to_key(p);
    int piece_offset = 64*zobrist_piece + 8*rank + file;
    return zobrist_hash_keys[piece_offset];
}

// Castling keys start at index 768, and go KQkq
// Castling offset starts at 0 and goes KQkq
Zobrist_Hash zobrist_hashing_hash_for_castle(int castling_offset) {
    castling_offset += 768;
    return zobrist_hash_keys[castling_offset];
}

Zobrist_Hash zobrist_hashing_hash_for_en_passant(Coord co) {
    if (!coord_is_valid(co))
        return 0;
    int en_passant_offset = co & FILE_MASK;
    en_passant_offset += 772;
    return zobrist_hash_keys[en_passant_offset];
}

// if it's black's turn, the hash is 0
Zobrist_Hash zobrist_hashing_hash_for_white() {
    return zobrist_hash_keys[780];
}

void _test_polyglot_hash_helper(Zobrist_Hash test_zh) {
    Zobrist_Hash zh = zobrist_hashing_generate_polyglot_hash_from_board();
    mu_assert(zh == test_zh);
}

void test_polyglot_hash() {
    context_init();

    // taken from the polyglot description document
    board_set_from_fen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
    _test_polyglot_hash_helper(U64(0x463b96181691fc9c));

    board_set_from_fen("rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1");
    _test_polyglot_hash_helper(U64(0x823c9b50fd114196));

    board_set_from_fen("rnbqkbnr/ppp1pppp/8/3p4/4P3/8/PPPP1PPP/RNBQKBNR w KQkq d6 0 2");
    _test_polyglot_hash_helper(U64(0x0756b94461c50fb0));

    board_set_from_fen("rnbqkbnr/ppp1pppp/8/3pP3/8/8/PPPP1PPP/RNBQKBNR b KQkq - 0 2");
    _test_polyglot_hash_helper(U64(0x662fafb965db29d4));

    board_set_from_fen("rnbqkbnr/ppp1p1pp/8/3pPp2/8/8/PPPP1PPP/RNBQKBNR w KQkq f6 0 3");
    _test_polyglot_hash_helper(U64(0x22a48b5a8e47ff78));

    board_set_from_fen("rnbqkbnr/ppp1p1pp/8/3pPp2/8/8/PPPPKPPP/RNBQ1BNR b kq - 0 3");
    _test_polyglot_hash_helper(U64(0x652a607ca3f242c1));

    board_set_from_fen("rnbq1bnr/ppp1pkpp/8/3pPp2/8/8/PPPPKPPP/RNBQ1BNR w - -0 4");
    _test_polyglot_hash_helper(U64(0x00fdd303c946bdd9));

    board_set_from_fen("rnbqkbnr/p1pppppp/8/8/PpP4P/8/1P1PPPP1/RNBQKBNR b KQkq c3 0 3");
    _test_polyglot_hash_helper(U64(0x3c8123ea7b067637));

    board_set_from_fen("rnbqkbnr/p1pppppp/8/8/P6P/R1p5/1P1PPPP1/1NBQKBNR b Kkq - 0 4");
    _test_polyglot_hash_helper(U64(0x5c3f9b829b279560));

    context_destroy();
}