#pragma once
#include "types.h"

inline bool castling_check_kingside(Stack_Frame * s, Color color) {
    return (color == WHITE) ? s->can_castle[0] : s->can_castle[2];
}

inline bool castling_check_queenside(Stack_Frame * s, Color color) {
    return (color == WHITE) ? s->can_castle[1] : s->can_castle[3];
}

inline void castling_set_kingside(Stack_Frame * s, Color color, bool value) {
    if (color == WHITE) {
        s->can_castle[0] = value;
    } else {
        s->can_castle[2] = value;
    }
}

inline void castling_set_queenside(Stack_Frame * s, Color color, bool value) {
    if (color == WHITE) {
        s->can_castle[1] = value;
    } else {
        s->can_castle[3] = value;
    }
}