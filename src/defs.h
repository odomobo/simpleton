// This file is to be included at the top of each source file. It includes every other
// header file, no other local header files should be included in any source file.
#pragma once

#ifdef _MSC_VER
#  define inline __inline
#  define U64(u) (u##ui64)
#else
#  define U64(u) (u##ULL)
#endif

// set up all types and forward declarations
#include "types.h"

// c is defined in the main source file
extern Context c;

#include "config.h"
#include "debug.h"
#include "myunit.h"
#include "utils.h"
#include "repl.h"
#include "generate_moves.h"
#include "evaluation.h"
#include "interruption.h"
#include "coords.h"
#include "moves.h"
#include "zobrist_hashing.h"
#include "board.h"
#include "move_vector.h"
#include "pv_table.h"
#include "search.h"
#include "context.h"
#include "optimizations.h"
#include "xboard.h"
#include "help.h"
#include "result.h"
#include "history_table.h"

// depends on optimizations
#include "pieces.h"

// depends on pieces
#include "castling.h"