// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include "types.h"

// These can be updated to reflect your project.
char const * const CONFIG_PROGRAM_NAME = "Simpleton";
char const * const CONFIG_AUTHOR_NAME = "Josh Odom";

const int CONFIG_VERSION_MAJOR = 0;
// Note that odd minor versions are development builds, while even minor versions are release builds.
const int CONFIG_VERSION_MINOR = 3;

// Only check to see if an interrupt has occurred for every 1 in n nodes processed.
// This should be a power of 2.
const int CONFIG_INTERRUPT_FREQUENCY = 8192;

// Run the timer for this many seconds less than what was specified.
// This is because we will always run over by a bit anyhow.
const float CONFIG_INTERRUPT_TIMER_FUDGE_FACTOR = 0.05f;

// 0x400000 is roughly equal to 4 million, for a total of 64 megabytes
const int CONFIG_PV_TABLE_SIZE = 0x400000;

// 128 seems like more than enough
const int CONFIG_STACK_SIZE = 128;

// This has to be larger than the number of half-moves in any game
const int CONFIG_HISTORY_SIZE = 4096;

// this doesn't need to be huge, just big enough to prevent most hash collisions of all moves in a game
const int CONFIG_HISTORY_TABLE_SIZE = 256;