// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include "types.h"

long long   utils_get_time_milliseconds();
void        utils_display_performance(float time);
extern int  utils_test_counter;