#include "defs.h"

void history_table_clear() {
    for (int i = 0; i < CONFIG_HISTORY_TABLE_SIZE; i++) {
        c.history_table[i] = NULL;
    }
}

void history_table_add(Stack_Frame * s) {
    // first find the number of repetitions
    int repetition_count;
    Stack_Frame * last_frame = history_table_find(s->zh);
    
    // if this is the first time we've seen it, then this is the first repetition
    if (!last_frame) {
        repetition_count = 1;
    } else if (last_frame->is_history) {
        // if it's in the history stack, that means we can use its repetition count
        repetition_count = last_frame->repetition_count + 1;
    } else {
        // If it's not in the history (i.e. is in the search stack), we're going to use an optimization that
        // tells the search algorithm that this is essentially a drawing position
        repetition_count = 3;
    }
    s->repetition_count = repetition_count;

    // then add this stack frame to the history table
    int index = s->zh & HISTORY_TABLE_MASK;
    s->history_table_next = c.history_table[index];
    c.history_table[index] = s;
}

// we should be able to remove an item even from the middle of a chain
void history_table_remove(Stack_Frame * rem) {
    int index = rem->zh & HISTORY_TABLE_MASK;
    Stack_Frame ** s_ptr = &c.history_table[index];
    while (*s_ptr) {
        // if we find it, remove it
        if (*s_ptr == rem) {
            *s_ptr = rem->history_table_next;
            return;
        }
        // otherwise try the next one
        s_ptr = &(*s_ptr)->history_table_next;
    }
    assert(false); // if we get here, that means the stack frame could not be found
}

Stack_Frame * history_table_find(Zobrist_Hash zh) {
    int index = zh & HISTORY_TABLE_MASK;
    Stack_Frame * s = c.history_table[index];
    while (s) {
        if (s->zh == zh)
            return s;
        s = s->history_table_next;
    }
    return NULL;
}

// returns true if the history table isn't corrupt
bool history_table_verify() {
    int history_table_count = 0;
    // count every entry in the history table
    for (int i = 0; i < CONFIG_HISTORY_TABLE_SIZE; i++) {
        Stack_Frame ** s_ptr = &c.history_table[i];
        while (*s_ptr) {
            history_table_count++;
            s_ptr = &(*s_ptr)->history_table_next;
        }
    }

    // ensure that it matches the count of stack frames
    int stack_frame_count = c.history_size + c.stack_depth + 1;
    if (stack_frame_count != history_table_count) {
        debug_int(stack_frame_count);
        debug_int(history_table_count);
        return false;
    }
    return true;
}

void test_history_table() {
    context_init(); mu_assert(history_table_verify());

    Stack_Frame * s1 = stack_frame_get();
    Zobrist_Hash zh1 = s1->zh;
    mu_assert(history_table_find(zh1) == s1);
    Move m1;
    generate_moves(true, false);
    board_find_move_from_string(&m1, "b1a3", false);
    context_perform_move_and_push_stack(&m1); mu_assert(history_table_verify());
    Stack_Frame * s2 = stack_frame_get();
    mu_assert(history_table_find(s2->zh) == s2);
    mu_assert(s2->repetition_count == 1);
    Move m2 = MOVE_EMPTY;
    // push 2 null moves, which should result in a a threefold repetition
    context_perform_move_and_push_stack(&m2); mu_assert(history_table_verify());
    context_perform_move_and_push_stack(&m2); mu_assert(history_table_verify());
    Stack_Frame * s3 = stack_frame_get();
    mu_assert(s3->repetition_count >= 3);
    mu_assert(history_table_find(s2->zh) == s3);
    context_perform_move_and_pop_stack(&m2); mu_assert(history_table_verify());
    context_perform_move_and_pop_stack(&m2); mu_assert(history_table_verify());
    // pop back and check that we can still find the correct stack frame
    mu_assert(history_table_find(s2->zh) == s2);
    // pop the original move and ensure it's no longer in the history table
    context_perform_move_and_pop_stack(&m1); mu_assert(history_table_verify());
    mu_assert(history_table_find(s2->zh) == NULL);

    // now commit the move and ensure that it can be found
    board_commit_move(&m1); mu_assert(history_table_verify());
    mu_assert(history_table_find(zh1) == &c.history[0]);
    // and undo it and ensure it can be found again
    board_undo(); mu_assert(history_table_verify());
    mu_assert(history_table_find(zh1) == s1);

    // finally commit a null move and ensure that the initial position can be found
    board_commit_move(&m2); mu_assert(history_table_verify());
    mu_assert(history_table_find(zh1) == &c.history[0]);
    // push a null move, and ensure that the new position is the 2nd repetition
    context_perform_move_and_push_stack(&m2); mu_assert(history_table_verify());
    Stack_Frame * s4 = stack_frame_get();
    mu_assert(history_table_find(zh1) == s4);
    mu_assert(s4->repetition_count == 2);
    // undo back to the original position, and ensure everything is good
    context_perform_move_and_pop_stack(&m2); mu_assert(history_table_verify());
    board_undo(); mu_assert(history_table_verify());
    mu_assert(history_table_find(zh1) == s1);

    // perform a move, then init the board to a different state, and verify
    board_commit_move(&m1); mu_assert(history_table_verify());
    board_set_by_name("perft1"); mu_assert(history_table_verify());
    Stack_Frame * s5 = stack_frame_get();
    mu_assert(history_table_find(s5->zh) == s5);

    context_destroy();
}