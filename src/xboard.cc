#include <string.h>
#include "defs.h"

// private
static void _xboard_search();
static bool _xboard_check_result();
// end private

bool xboard_post = false;
bool xboard_engine_is_playing = true;
Color xboard_engine_color = BLACK;

// Set to some sane defaults; the GUI will update these
bool xboard_use_time_remaining = false;
float xboard_time_remaining;
bool xboard_use_fixed_time_per_turn = false;
float xboard_fixed_time_per_turn;
int xboard_search_depth = 9999;

void xboard_protover_repl(char const * arg) {
    int version = atoi(arg);
    assert(version == 2);
    printf("feature ping=1 setboard=1 colors=0 usermove=1 draw=0 sigint=0 sigterm=0 analyze=0 nps=0 debug=1\n");
    printf("feature done=1\n");
}

void xboard_new_repl(char const * arg) {
    board_init();
    xboard_engine_is_playing = true;
    xboard_engine_color = BLACK;
    xboard_use_time_remaining = false;
    xboard_use_fixed_time_per_turn = false;
    xboard_search_depth = 9999;
}

void xboard_st_repl(char const * arg) {
    int seconds = atoi(arg);
    if (seconds <= 0 || seconds >= 9999) {
        xboard_use_fixed_time_per_turn = false;
    } else {
        xboard_use_fixed_time_per_turn = true;
        xboard_use_time_remaining = false; // only 1 can be enabled at a time
        xboard_fixed_time_per_turn = seconds;
    }
}

// Basically we ignore what level tells us; it'll tell us "time" anyhow.
// TODO: eventually, use the information that is given here.
void xboard_level_repl(char const * arg) {
    xboard_use_time_remaining = false;
    xboard_use_fixed_time_per_turn = false;
}

void xboard_time_repl(char const * arg) {
    int centiseconds = atoi(arg);
    xboard_use_time_remaining = true;
    xboard_use_fixed_time_per_turn = false; // only 1 can be enabled at a time
    xboard_time_remaining = centiseconds / 100.0f;
}

void xboard_sd_repl(char const * arg) {
    int depth = atoi(arg);
    if (depth <= 0) {
        xboard_search_depth = 9999;
    } else {
        xboard_search_depth = depth;
    }
}

void xboard_force_repl(char const * arg) {
    xboard_engine_is_playing = false;
}

void xboard_go_repl(char const * arg) {
    xboard_engine_is_playing = true;
    xboard_engine_color = c.to_move;
    // check if the game is over; if so, give an error
    generate_moves(false, false);
    if (result_check(false) != RESULT_NONE) {
        printf("Error (game over): go\n");
        return;
    }
    
    _xboard_search();
}

void xboard_usermove_repl(char const * arg) {
    generate_moves(true, false);
    if (result_check(false) != RESULT_NONE) {
        printf("Error (game over): usermove\n");
        return;
    }
    Move m;
    if (!board_find_move_from_string(&m, arg, false)) {
        printf("Illegal move: %s\n", arg);
        return;
    }

    board_commit_move(&m);
    if (_xboard_check_result())
        return;
    if (xboard_engine_is_playing && c.to_move == xboard_engine_color)
        _xboard_search();
}

void xboard_undo_repl(char const * arg) {
    if (c.history_size > 0) {
        board_undo();
    } else {
        printf("Error (no previous moves): undo\n");
    }
}

void xboard_remove_repl(char const * arg) {
    if (c.history_size >= 2) {
        board_undo();
        board_undo();
    } else {
        printf("Error (not enough previous moves): remove\n");
    }
}

void xboard_ping_repl(char const * arg) {
    int i = atoi(arg);
    printf("pong %d\n", i);
}

void xboard_post_repl(char const * arg) {
    xboard_post = true;
}

void xboard_nopost_repl(char const * arg) {
    xboard_post = false;
}

// returns true if the game is over
static bool _xboard_check_result() {
    // need to generate moves in order to call result_check()
    generate_moves(false, false);
    Result result = result_check(false);
    switch (result) {
    case RESULT_NONE:
        return false;
    case RESULT_CHECKMATE:
        if (c.to_move == WHITE) {
            printf("0-1 {Black mates}\n");
        } else {
            printf("1-0 {White mates}\n");
        }
        return true;
    case RESULT_STALEMATE:
        printf("1/2-1/2 {Stalemate}\n");
        return true;
    case RESULT_INSUFFICIENT_MATERIAL:
        printf("1/2-1/2 {Draw by insufficient material}\n");
        return true;
    case RESULT_REPETITION:
        printf("1/2-1/2 {Draw by 3-move repetition}\n");
        return true;
    case RESULT_50_MOVE:
        printf("1/2-1/2 {Draw by 50 move rule}\n");
        return true;
    default:
        debug("Uncaught result: %d\n", result);
        return true;
    }
}

static void _xboard_search() {
    Scored_Move sm;

    // set the interruption time, if there is one
    interruption_clear_interrupt();
    if (xboard_use_time_remaining) {
        interruption_set_timer(xboard_time_remaining / 30.0f);
    } else if (xboard_use_fixed_time_per_turn) {
        interruption_set_timer(xboard_fixed_time_per_turn);
    }

    long long t1 = utils_get_time_milliseconds();
    search(xboard_search_depth, &sm);
    long long t2 = utils_get_time_milliseconds();
    float elapsed_time = (t2 - t1) / 1000.0f;
    utils_display_performance(elapsed_time);
    evaluation_evaluate_position_repl(NULL);

    // update the interruption time; shouldn't be needed, as the GUI will normally tell us the updated interruption time
    if (xboard_use_time_remaining)
        xboard_time_remaining -= elapsed_time;

    board_commit_move(&sm);
    char * move_str = move_to_string(&sm, false);
    printf("move %s\n", move_str);
    free(move_str);
    _xboard_check_result();
}

// only free the memory if required for correctness (i.e. during debugging)
void xboard_quit_repl(char const * arg) {
    context_destroy();
    exit(0);
}