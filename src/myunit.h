// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
// loosely based on minunit

#include <stdio.h>
#include <stdlib.h>

#define mu_assert(test)                                                                 \
    do {                                                                                \
        if (! (test) ) {                                                                \
            printf("%s:%d:%s(): Test failed: %s\n",                                     \
                    __FILE__, __LINE__, __FUNCTION__, #test);                           \
            printf("Tests run: %d\n", tests_passed);                                    \
            exit(1);                                                                    \
        }                                                                               \
    }                                                                                   \
    while(0)

extern int tests_passed;