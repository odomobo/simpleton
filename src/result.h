#pragma once
#include "types.h"

const Result RESULT_NONE        = 0;
const Result RESULT_CHECKMATE   = 1;
const Result RESULT_STALEMATE   = 2;
const Result RESULT_INSUFFICIENT_MATERIAL = 3;
const Result RESULT_REPETITION  = 4;
const Result RESULT_50_MOVE     = 5;

extern Score result_scores[];

Result result_check(bool quiesce);