// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once

// types
typedef int Coord;
typedef int Offset;
typedef int Piece_Type;
typedef int Color;
typedef int Piece;
typedef float Score;
typedef unsigned long long Zobrist_Hash;
typedef unsigned short Compact_Move;
typedef int Result;

// forward declarations
struct Context;
struct Stack_Frame;
struct Move_Component;
struct Move;
struct Scored_Move;
struct Move_Vector;
struct PV_Entry;
struct Evaluation_Table_Entry;
struct Evaluation_Info;