#include <stdio.h>
#include <string.h>
#include "defs.h"

Move_Component & Move::operator[] (const int index) {
    return _components[index];
}

// memory must be freed when it's done being used
char * move_to_string(Move * m, bool extended) {
    char * ret = (char *)malloc(10);

    char move_piece_str[2] = "";
    Piece_Type move_piece = piece_get_type((*m)[0].parity);
    if (extended) {
        if (move_piece != PAWN) {
            sprintf(move_piece_str, "%c", piece_table[move_piece].display[0]);
        }
    }
    
    char * from_move = coord_to_string((*m)[0].coord);
    char separator = move_is_capture(m) ? 'x' : '-';
    char * to_move = coord_to_string((*m)[1].coord);

    // handle pawn promotions correctly
    char promotion_piece_str[2] = "";
    if (move_is_pawn_promotion(m)) {
        Piece_Type promoted_piece = piece_get_type((*m)[3].parity);
        sprintf(promotion_piece_str, "%c", piece_table[promoted_piece].display[1]); // show the piece in lowercase
    }

    if (extended) {
        sprintf(ret, "%s%s%c%s%s", move_piece_str, from_move, separator, to_move, promotion_piece_str);
    } else {
        sprintf(ret, "%s%s%s", from_move, to_move, promotion_piece_str);
    }

    free(from_move);
    free(to_move);

    return ret;
}

void move_display(Move * m, bool extended) {
    char * s = move_to_string(m, extended);
    printf("%s\n", s);
    free(s);
}

// Function to perform move. Does not update the stack by change sides or anything like that.
// Note that this function will also undo a move if it has already been performed.
void move_perform(Move * m) {
    struct Move_Component * co = &(*m)[0];
    for (int i = 0; i < MOVE_COMPONENTS_PER_MOVE; i++) {
        // this is very important, because in the case of promotions, the 3rd component is blank, but 
        // the 4th component is the piece being promoted
        if (co[i].parity == 0)
            break;
        c.board[(int)co[i].coord] ^= co[i].parity;
    }
}

// Test function for move_perform.
// This works by initializing the board, making a move, checking the state, undoing the move, and checking that the
// new state is identical to the original state.
void test_perform_move() {
    context_init();

    Piece board[128];
    memcpy(board, &c.board[0], sizeof(board));

    Move m;
    generate_moves(false, false);
    board_find_move_from_string(&m, "e2e4", false);

    // do move
    move_perform(&m);
    mu_assert(piece_get(1, 4) == SPACE);
    mu_assert(piece_get(3, 4) == (PAWN|WHITE));

    // undo move
    move_perform(&m);
    mu_assert(memcmp(board, &c.board[0], sizeof(board)) == 0);
    context_destroy();
}

bool moves_do_match(Move * m1, Move * m2) {
    for (int i = 0; i < 4; i++) {
        if ((*m1)[i].parity != m2->_components[i].parity)
            return false;
        // this has to be done before checking the square, because although there is guaranteed a sentinel parity
        // value, no such guarantee is made for the square
        if ((*m1)[i].parity == 0)
            break;
        if ((*m1)[i].coord != m2->_components[i].coord)
            return false;
    }
    // pawns also require matching promotion bits
    if (piece_get_type((*m1)[0].parity) == PAWN) {
        if ((*m1)[3].parity != m2->_components[3].parity)
            return false;
    }
    return true;
}

bool move_is_capture(Move * m) {
    Piece p = (*m)[0].parity;
    if (piece_get_type(p) != PAWN) {
        return p != (*m)[1].parity;
    } else {
        // pawn logic is any diagonal move is an attack
        return ((*m)[0].coord & FILE_MASK) != ((*m)[1].coord & FILE_MASK);
    }
    return false;
}

Piece_Type move_get_capture_victim(Move * m) {
    Piece_Type attacker = piece_get_type((*m)[0].parity);
    Piece_Type victim = piece_get_type((*m)[1].parity ^ (*m)[0].parity);
    if (attacker == PAWN) {
        // if non-en-passant
        if ((*m)[2].parity == 0) {
            // this also works for capturing into promotion
            victim = piece_get_type((*m)[3].parity ^ (*m)[1].parity);
        } else {
            // en passant
            victim = PAWN;
        }
    }
    return victim;
}

bool move_is_pawn_promotion(Move * m) {
    // if not a pawn, then it's definitely not a promotion
    if (piece_get_type((*m)[0].parity) != PAWN)
        return false;

    // if it's not an en-passant, and the end piece isn't a pawn, then it's a promotion
    if ((*m)[2].parity == 0 && piece_get_type((*m)[3].parity) != PAWN) {
        return true;
    } else {
        return false;
    }
}

void test_is_move_a_pawn_promotion() {
    context_init();
    Stack_Frame * s = stack_frame_get();

    generate_moves(true, false);
    for (int i = 0; i < s->moves.count; i++) {
        mu_assert(move_is_pawn_promotion(&s->moves[i]) == false);
    }

    board_set_by_name("test1");
    generate_moves(true, false);
    for (int i = 0; i < s->moves.count; i++) {
        bool is_pawn = piece_get_type(s->moves[i][0].parity) == PAWN;
        mu_assert(move_is_pawn_promotion(&s->moves[i]) == is_pawn);
    }

    context_destroy();
}

// A compact move can fit in a short int (16 bits) because
// it only records the source and destination coordinates.
// An individual coordinate fits in 8 bits.
Compact_Move move_to_compact_move(Move * m) {
    Compact_Move ret = (Compact_Move)(*m)[0].coord;
    ret |= (Compact_Move)(*m)[1].coord << 8;
    return ret;
}

// this will only work if moves have already been generated for the current position
void move_find_from_compact_move(Move * dst, Compact_Move query) {
    Stack_Frame * s = stack_frame_get();
    for (int i = 0; i < s->moves.count; i++) {
        if (move_to_compact_move(&s->moves[i]) == query) {
            *dst = s->moves[i];
            return;
        }
    }
    // if it made it this far, the search has failed; set as a null move
    memset(dst, 0, sizeof(Move));
}

void test_copy_compact_move() {
    context_init();
    Stack_Frame * s = stack_frame_get();

    board_set_by_name("perft1");
    generate_moves(false, false);
    // make sure compact moves match for matching moves, and don't match for non-matching moves
    for (int i = 0; i < s->moves.count; i++) {
        for (int j = i; j < s->moves.count; j++) {
            if (i == j) {
                mu_assert(move_to_compact_move(&s->moves[i]) == move_to_compact_move(&s->moves[j]));
            } else {
                mu_assert(move_to_compact_move(&s->moves[i]) != move_to_compact_move(&s->moves[j]));
            }
        }
    }

    context_destroy();
}