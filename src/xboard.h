#pragma once
#include "types.h"

extern bool xboard_post;
extern bool xboard_engine_is_playing;
extern Color xboard_engine_color;
extern float xboard_time_remaining;

void xboard_protover_repl(char const * arg);
void xboard_new_repl(char const * arg);
void xboard_st_repl(char const * arg);
void xboard_level_repl(char const * arg);
void xboard_time_repl(char const * arg);
void xboard_sd_repl(char const * arg);
void xboard_force_repl(char const * arg);
void xboard_go_repl(char const * arg);
void xboard_usermove_repl(char const * arg);
void xboard_undo_repl(char const * arg);
void xboard_remove_repl(char const * arg);
void xboard_ping_repl(char const * arg);
void xboard_post_repl(char const * arg);
void xboard_nopost_repl(char const * arg);
void xboard_quit_repl(char const * arg);