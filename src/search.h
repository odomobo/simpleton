// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include "types.h"

void search(int depth, Scored_Move * sm);
void search_repl(char const * depth_str);