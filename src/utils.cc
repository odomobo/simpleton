#include <sys/timeb.h>
#include "defs.h"

// only used for testing
int utils_test_counter = 0;

long long utils_get_time_milliseconds() {
    struct timeb t;
    ftime(&t);

    return (t.time * 1000) + t.millitm;
}

void utils_display_performance(float time) {
    if (time > 0.005) {
        int nodes_per_second = (int)(c.counter / time);
        int thousand_nodes_per_second = nodes_per_second / 1000.0f;
        printf("# %d nodes in %.3f seconds, for %dk nodes per second\n", c.counter, time, thousand_nodes_per_second);
    } else {
        printf("# %d nodes in %.3f seconds, for ??? nodes per second\n", c.counter, time);
    }
}