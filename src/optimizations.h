// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include "types.h"

extern bool optimizations_piece_can_move_like_table[7][256];

void        optimizations_init();