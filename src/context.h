// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include "types.h"

struct Stack_Frame {
    Move_Vector moves;
    bool can_castle[4]; // white kingside, queenside; black kingside, queenside
    Coord en_passant;
    bool in_check;
    int halfmoves_since_capture; // actually, since capture or pawn movement. This is for the 50 move rule
    Zobrist_Hash zh;

    // history-specific
    bool is_history;
    Move history_move;

    // history-table-specific
    Stack_Frame * history_table_next;
    int repetition_count;

    // stateful data
    Move killer_moves[2];
};

// The global Context for calculations. By keeping all global state in a struct, it's trivial to parallelize the code.
struct Context {
    Piece board[128];
    Color to_move;
    int total_halfmoves; // this is the move counter, but in half moves since the start of the game
    int stack_depth;
    Stack_Frame stack[CONFIG_STACK_SIZE];
    int history_size;
    Stack_Frame history[CONFIG_HISTORY_SIZE];

    Stack_Frame * history_table[CONFIG_HISTORY_TABLE_SIZE];

    // informational
    int counter;

    // this can be removed once piece lists are implemented
    Coord king_position_cache[2];

    // this needs to be changed to be shard
    PV_Entry pv_table[CONFIG_PV_TABLE_SIZE];
};

void        context_init();
void        context_destroy();
void        context_clear_state();
void        context_perform_move_and_push_stack(Move * m);
void        context_perform_move_and_pop_stack(Move * m);

inline Stack_Frame * stack_frame_get() {
    return &c.stack[c.stack_depth];
}
void        stack_frame_move(Stack_Frame * dst, Stack_Frame * src);