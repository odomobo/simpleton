#include <stdio.h>
#include <string.h>
#include "defs.h"

Predefined_Board predefined_boards[] = {
#include "boards.dat"
};

// private
static int _total_halfmoves_from_move_number_color(int move_number, Color to_move);
static int _move_number_from_total_halfmoves(int total_halfmoves);
// end private

void board_init() {
    board_set_by_name("init");
}

// unused
void board_init_repl(char const * arg) {
    board_init();
}

void board_commit_move(Move * m) {
    assert(c.stack_depth == 0);
    // first we need to copy the current stack frame into history and store the move
    Stack_Frame * h = &c.history[c.history_size];
    stack_frame_move(h, &c.stack[0]);
    h->is_history = true;
    h->history_move = *m;
    c.history_size++;

    // perform the move onto stack frame 1, then copy that stack frame to 0 and set the stack depth to 0
    context_perform_move_and_push_stack(m);
    stack_frame_move(&c.stack[0], &c.stack[1]);
    c.stack_depth = 0;
}

void board_undo() {
    assert(c.stack_depth == 0);
    assert(c.history_size > 0);

    // copy stack frame 0 to 1, so it can be popped
    stack_frame_move(&c.stack[1], &c.stack[0]);

    // copy the history stack frame to 0
    c.history_size--;
    Stack_Frame * s = stack_frame_get();
    stack_frame_move(s, &c.history[c.history_size]);
    s->is_history = false;

    // then pop to undo back to the previous position
    c.stack_depth = 1;
    context_perform_move_and_pop_stack(&s->history_move);
}

void test_undo() {
    context_init();
    // only do a very simple test; try castling then undo
    const char * fen = "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1";
    board_set_from_fen(fen);
    Move m;
    board_find_move_from_string(&m, "e1g1", false);
    board_commit_move(&m);
    board_undo();
    char * fen2 = board_get_fen();
    mu_assert(strcmp(fen, fen2) == 0);
    free(fen2);

    context_destroy();
}

void board_display_repl(char const * arg) {
    Stack_Frame * s = stack_frame_get();
    printf("\n");
    for (int rank = 7; rank >= 0; rank--) {
        printf("%d  ", rank+1);
        for (int file = 0; file < 8; file++) {
            printf(" %c", piece_to_display(piece_get(rank, file)));
        }
        printf("\n");
    }
    printf("\n    a b c d e f g h\n");
    printf("To Move: %s\n", (c.to_move == WHITE) ? "White" : "Black");
    printf("Castling: ");
    printf("%c", s->can_castle[0] ? 'K' : '-');
    printf("%c", s->can_castle[1] ? 'Q' : '-');
    printf("%c", s->can_castle[2] ? 'k' : '-');
    printf("%c", s->can_castle[3] ? 'q' : '-');
    printf("\n");
    if (coord_is_valid(s->en_passant)) {
        char * ep_str = coord_to_string(s->en_passant);
        printf("En Passant: %s\n", ep_str);
        free(ep_str);
    } else {
        printf("En Passant: --\n");
    }
    printf("Zobrist Hash: 0x%016llx\n", s->zh);
    printf("Move number: %d\n", (c.total_halfmoves/2) + 1);
    printf("Halfmoves since capture/promotion: %d\n", s->halfmoves_since_capture);
    printf("Position repetitions: %d\n", s->repetition_count);
}

void board_display_moves(bool extended, bool quiesce) {
    generate_moves(true, quiesce);
    Stack_Frame * s = stack_frame_get();
    int ctr = 0;
    for (int i = 0; i < s->moves.count; i++) {
        char * move_str = move_to_string(&s->moves[i], extended);

        if (ctr + 10 > 80) {
            printf("\n");
            ctr = 0;
        }

        printf("%s,\t", move_str);
        free(move_str);
        ctr += 10;
    }
    printf("\n");
}

void board_display_moves_repl(char const * arg) {
    if (strcmp(arg, "simple") == 0) {
        board_display_moves(false, false);
    } else if (strcmp(arg, "quiesce") == 0) {
        board_display_moves(true, true);
    } else {
        board_display_moves(true, false);
    }
}

// return value is success value
bool board_set_by_name(char const * name) {
    struct Predefined_Board * p = predefined_boards;
    
    // an empty string acts as a sentinel, showing the end of the list of boards
    while (p->name[0] != '\0') {
        if (strncmp(p->name, name, 16) == 0) {
            return board_set_from_fen(&p->fen[0]);
        }
        
        p++;
    }
    
    return false; // failure
}

void board_set_by_name_repl(char const * name) {
    bool success = board_set_by_name(name);
    if (!success) {
        printf("Named board not found.\n");
        board_init();
    }
}

bool board_set_from_fen(char const * fen) {
    // reset some values
    c.stack_depth = 0;
    c.history_size = 0;
    pv_table_clear();

    char pieces[128]; // max should be 64+7+1, but whatever
    char to_move;
    char can_castle[8]; // 4 castle options + null character
    char en_passant[8]; // 2 coords + null character
    int halfmoves, move_number; // unused until I actually have space on the stack for them
    sscanf(fen, "%s %c %s %s %d %d", pieces, &to_move, can_castle, en_passant, &halfmoves, &move_number);

    // parse pieces
    int skip = 0;
    char * p = pieces;
    for (int rank = 7; rank >= 0; rank--) {
        for (int file = 0; file < 8; file++) {
            if (skip > 0) {
                piece_set(rank, file, SPACE);
                skip--;
                continue;
            }
            char piece = *p++;
            if (piece > '0' && piece <= '8') {
                skip = piece - '0';
                piece_set(rank, file, SPACE);
                skip--;
                continue;
            }
            int i;
            for (i = 1; i < 7; i++) {
                if (piece_table[i].display[0] == piece) {
                    piece_set(rank, file, (i|WHITE));
                    break;
                }
                if (piece_table[i].display[1] == piece) {
                    piece_set(rank, file, (i|BLACK));
                    break;
                }
            }
            if (i < 7)
                continue;
            if (piece == '\0') {
                printf("Fen pieces string is too short.\n");
            } else {
                printf("Unexpected fen character: %c\n", piece);
            }
            return false;
        }
        if (*p == '/') {
            p++;
        } else if (*p == '\0' && rank == 0) {
            // success
        } else {
            printf("Found char '%c' when expecting '/'\n", *p);
            return false;
        }
    }
    Stack_Frame * s = stack_frame_get();
    if (to_move == 'w') {
        c.to_move = WHITE;
    } else {
        c.to_move = BLACK;
    }
    // castling
    for (int i = 0; i < 4; i++) {
        s->can_castle[i] = false;
    }
    char * cas = can_castle;
    while (*cas) {
        switch (*cas++) {
        case 'K':
            s->can_castle[0] = true;
            break;
        case 'Q':
            s->can_castle[1] = true;
            break;
        case 'k':
            s->can_castle[2] = true;
            break;
        case 'q':
            s->can_castle[3] = true;
            break;
        case '-':
            break;
        default:
            printf("Bad castle character: %c\n", *(--cas));
            return false;
        }
    }
    // en passant
    if (en_passant[0] == '-') {
        s->en_passant = COORD_INVALID;
    } else if (en_passant[0] >= 'a' && en_passant[0] <= 'h' && en_passant[1] >= '1' && en_passant[1] <= '8') {
        s->en_passant = coord_get(en_passant[1] - '1', en_passant[0] - 'a');
    } else {
        printf("Bad en passant\n");
        return false;
    }
    s->moves.clear();

    s->halfmoves_since_capture = halfmoves;
    c.total_halfmoves = _total_halfmoves_from_move_number_color(move_number, c.to_move);

    // it's important that these are done last
    s->in_check = board_is_king_attacked(c.to_move);
    s->zh = zobrist_hashing_generate_from_board();
    history_table_clear();
    history_table_add(s);
    return true;
}

void board_set_from_fen_repl(char const * fen) {
    bool success = board_set_from_fen(fen);
    if (!success) {
        printf("Bad fen string.\n");
        board_init();
    }
}

// Ensure that fen is being processed correctly.
void test_set_board() {
    context_init();
    Stack_Frame * s = stack_frame_get();

    // check the 2 corners, the white queen, and a random space; assume everything else is fine
    mu_assert(piece_get(0, 0) == (ROOK|WHITE));
    mu_assert(piece_get(7, 7) == (ROOK|BLACK));
    mu_assert(piece_get(0, 3) == (QUEEN|WHITE));
    mu_assert(piece_get(3, 3) == SPACE);

    // now, make sure that boards get castling and other stuff set correctly
    mu_assert(c.to_move == WHITE);
    mu_assert(s->en_passant == COORD_INVALID);
    bool castle_KQkq[4] = { true, true, true, true };
    mu_assert(memcmp(&s->can_castle[0], castle_KQkq, sizeof(castle_KQkq)) == 0);

    // try other stuff with a new board
    board_set_by_name("BK.01");
    mu_assert(c.to_move == BLACK);
    mu_assert(s->en_passant == COORD_INVALID);
    bool castle_none[4] = { false, false, false, false };
    mu_assert(memcmp(&s->can_castle[0], castle_none, sizeof(castle_none)) == 0);

    // try different castling configurations
    board_set_by_name("BK.23");
    bool castle_kq[4] = { false, false, true, true };
    mu_assert(memcmp(&s->can_castle[0], castle_kq, sizeof(castle_kq)) == 0);

    // dummy fens to try castling configurations
    board_set_from_fen("r3k2r/8/8/8/8/8/8/R3K2R w Qk - 0 1");
    bool castle_Qk[4] = { false, true, true, false };
    mu_assert(memcmp(&s->can_castle[0], castle_Qk, sizeof(castle_Qk)) == 0);
    board_set_from_fen("r3k2r/8/8/8/8/8/8/R3K2R w Kq - 0 1");
    bool castle_Kq[4] = { true, false, false, true };
    mu_assert(memcmp(&s->can_castle[0], castle_Kq, sizeof(castle_Kq)) == 0);

    // check en passant is being set
    board_set_by_name("perft2");
    mu_assert(s->en_passant == 0x23); // d3
    context_destroy();
}

// Returns the fen representation of the board.
// Caller is responsible for freeing.
char * board_get_fen() {
    char pieces[128];
    char to_move;
    char can_castle[8];
    int move_number;

    // encode pieces
    int skipped = 0;
    char * p = pieces;
    for (int rank = 7; rank >= 0; rank--) {
        for (int file = 0; file < 8; file++) {
            Piece q = piece_get(rank, file);
            if (q == SPACE) {
                skipped++;
                continue;
            }

            if (skipped > 0) {
                *p++ = '0' + skipped;
                skipped = 0;
            }
            *p++ = piece_table[piece_get_type(q)].display[piece_get_color_index(q)];
        }
        if (skipped > 0) {
            *p = '0' + skipped;
            p++;
            skipped = 0;
        }
        if (rank > 0) {
            *p++ = '/';
        }
    }
    *p++ = '\0';

    Stack_Frame * s = stack_frame_get();
    to_move = (c.to_move == WHITE) ? 'w' : 'b';

    char * cas = can_castle;
    if (s->can_castle[0])
        *cas++ = 'K';
    if (s->can_castle[1])
        *cas++ = 'Q';
    if (s->can_castle[2])
        *cas++ = 'k';
    if (s->can_castle[3])
        *cas++ = 'q';
    // if we haven't added any castling conditions
    if (cas == can_castle)
        *cas++ = '-';
    *cas++ = '\0';

    char * en_passant;
    if (s->en_passant != COORD_INVALID) {
        en_passant = coord_to_string(s->en_passant);
    } else {
        // we have to dynamically allocate the memory for en_passant, because it's going to be unconditionally freed below
        en_passant = (char *)malloc(2);
        strcpy(en_passant, "-");
    }

    move_number = _move_number_from_total_halfmoves(c.total_halfmoves);

    char * fen = (char *)malloc(128);

    sprintf(fen, "%s %c %s %s %d %d", pieces, to_move, can_castle, en_passant, s->halfmoves_since_capture, move_number);
    free(en_passant);
    return fen;
}

void board_get_fen_repl(char const * arg) {
    char * fen = board_get_fen();
    printf("%s\n", fen);
    free(fen);
}

void test_get_board() {
    context_init();

    char fens[][128] = {
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
        "1k1r4/pp1b1R2/3q2pp/4p3/2B5/4Q3/PPP2B2/2K5 b - - 5 20",
        "3r1k2/4npp1/1ppr3p/p6P/P2PPPP1/1NR5/5K2/2R5 w - - 0 1",
        "r1bqk2r/pp2bppp/2p5/3pP3/P2Q1P2/2N1B3/1PP3PP/R4RK1 b kq - 0 1",
        "r3k2r/8/8/8/8/8/8/R3K2R w Qk - 0 1",
        "r3k2r/8/8/8/8/8/8/R3K2R w Kq - 0 1",
        "8/7p/p5pb/4k3/P1pPn3/8/P5PP/1rB2RK1 b - d3 0 1"
    };

    for (int i = 0; i < 7; i++) {
        board_set_from_fen(fens[i]);
        char * fen = board_get_fen();
        mu_assert(strcmp(fens[i], fen) == 0);
        free(fen);
    }

    context_destroy();
}



// Check if a given king is being attacked.
// Just find the square the given king is on, and then call the above function.
bool board_is_king_attacked(Color color) {
    Coord king_square = board_find_king(color);
    return coord_is_attacked(king_square, color);
}

// Test square and king attack functions.
// Set the king to every free square on a given board. Then match the results against a list of known results.
// Uses BK.03 board position to ensure all checks are correct.
void test_is_king_attacked() {
    context_init();
    board_set_by_name("BK.03");

    // all the squares which are attacked by BK.03
    char check_squares[] =
        "XXXXXXXX"
        ".XXXXXXX"
        "X.XXXXXX"
        ".XX.XXXX"
        "XX.X.XX."
        "X.X....."
        "........"
        "........";
    char * ck = check_squares;

    // the white king is initially at h2; we need to remove him
    piece_set(1, 7, SPACE);

    for (int rank = 7; rank >= 0; rank--) {
        for (int file = 0; file < 8; file++) {
            Piece bak = piece_get(rank, file);
            piece_set(rank, file, (KING|WHITE));
            if (board_is_king_attacked(WHITE) != (*ck++ == 'X')) {
                fprintf(stderr, "Bad check at rank: %d, file: %d\n", rank, file);
                mu_assert(false);
            }
            piece_set(rank, file, bak);
        }
    }
    context_destroy();
}

// Function to find the king.
// On failure, to find the king, return invalid coord.
Coord board_find_king(Color color) {
    Coord cache = c.king_position_cache[color_to_index(color)];
    if (coord_is_valid(cache)) {
        if (piece_get_by_coord(cache) == (KING|color)) {
            return cache;
        }
    }
    // optimization to choose most likely starting rank, per color
    // since kingside castling is most common, start from the h file and work down
    if (color == WHITE) {
        for (int rank = 0; rank < 8; rank++) {
            for (int file = 7; file >= 0; file--) {
                if (piece_get(rank, file) == (KING|color)) {
                    // update cache
                    cache = coord_get(rank, file);
                    c.king_position_cache[color_to_index(color)] = cache;
                    return cache;
                }
            }
        }
    } else {
        for (int rank = 7; rank >= 0; rank--) {
            for (int file = 7; file >= 0; file--) {
                if (piece_get(rank, file) == (KING|color)) {
                    // update cache
                    cache = coord_get(rank, file);
                    c.king_position_cache[color_to_index(color)] = cache;
                    return cache;
                }
            }
        }
    }
    debug("Error: failed to find the %x king.\n", color);
    return COORD_INVALID;
}

// Test function for finding the king.
void test_find_king() {
    context_init();

    Coord white_king = board_find_king(WHITE);
    mu_assert(white_king == coord_get(0, 4));

    Coord black_king = board_find_king(BLACK);
    mu_assert(black_king == coord_get(7, 4));
    context_destroy();
}

// Moves must have already been generated.
// Note that if the move was not found, then dst is in an undefined state!
bool board_find_move_from_string(Move * dst, char const * move_str, bool extended) {
    assert(dst != NULL);
    assert(move_str != NULL);
    Stack_Frame * s = stack_frame_get();
    for (int i = 0; i < s->moves.count; i++) {
        char * t_move_str = move_to_string(&s->moves[i], extended);
        int cmp = strcmp(t_move_str, move_str);
        free(t_move_str);
        if (cmp == 0) {
            *dst = s->moves[i];
            return true;
        }
    }
    // do this to prevent possible memory corruption in tests, but the real code should never be relying on this
    if (DEBUG > 0)
        *dst = MOVE_EMPTY;
    return false;
}

void test_find_move_from_string() {
    context_init();

    // check a few moves in perft1
    board_set_by_name("perft1");
    generate_moves(true, false);

    Move m;
    mu_assert(board_find_move_from_string(&m, "Ne5xd7", true) == true);
    mu_assert(move_is_capture(&m) == true);
    mu_assert(m[0].coord == 0x44);
    mu_assert(m[1].coord == 0x63);

    mu_assert(board_find_move_from_string(&m, "e5d7", false) == true);
    mu_assert(board_find_move_from_string(&m, "g2-g4", true) == true);
    mu_assert(board_find_move_from_string(&m, "Qf3-g4", true) == true);
    mu_assert(board_find_move_from_string(&m, "e1e2", false) == false);

    context_destroy();
}

// TODO: rename these to something more clear
static int _total_halfmoves_from_move_number_color(int move_number, Color to_move) {
    int total_halfmoves = (move_number - 1) * 2;
    // on white's turn, total_halfmoves is an even number
    if (to_move == BLACK)
        total_halfmoves += 1;
    return total_halfmoves;
}

static int _move_number_from_total_halfmoves(int total_halfmoves) {
    // if it's black's turn (an odd number), the division will round down to the correct value
    return (total_halfmoves / 2) + 1;
}