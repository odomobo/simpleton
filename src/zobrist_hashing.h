// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include "types.h"

extern const Zobrist_Hash zobrist_hash_keys[781];

Zobrist_Hash    zobrist_hashing_generate_from_board();
Zobrist_Hash    zobrist_hashing_generate_polyglot_hash_from_board();
int             zobrist_hashing_piece_to_key(Piece p);
Zobrist_Hash    zobrist_hashing_hash_for_piece(Piece p, int rank, int file);
Zobrist_Hash    zobrist_hashing_hash_for_castle(int castling_offset);
Zobrist_Hash    zobrist_hashing_hash_for_en_passant(Coord co);
Zobrist_Hash    zobrist_hashing_hash_for_white();
