#include "defs.h"

bool optimizations_piece_can_move_like_table[7][256];

// private
static void _optimizations_init_piece_can_move_like_table();
// end private

// the piece_can_move_like table provides a quick lookup, per piece, to see if it moves in a certain direction
static void _optimizations_init_piece_can_move_like_table() {
    for (Piece_Type p = 0; p < 7; p++) {
        for (Offset offset = 0; offset < 256; offset++) {
            optimizations_piece_can_move_like_table[p][offset] = false;
        }
        for (int i = 0; i < piece_table[p].move_direction_count; i++) {
            Offset offset = piece_table[p].move_directions[i];
            optimizations_piece_can_move_like_table[p][offset & 0xFF] = true;
        }
    }
}

void optimizations_init() {
    _optimizations_init_piece_can_move_like_table();
}

