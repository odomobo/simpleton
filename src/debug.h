// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include <stdio.h>

#ifndef DEBUG
#define DEBUG 0
#endif

#define assert(test)                                                                    \
    do {                                                                                \
        if (DEBUG>0) {                                                                  \
            if (! (test) ) {                                                            \
                printf("# %s:%d:%s(): Assert failed: %s\n",                             \
                        __FILE__, __LINE__, __FUNCTION__, #test);                       \
            }                                                                           \
        }                                                                               \
    }                                                                                   \
    while(0)

#define debug(fmt, ...)                                                                 \
    do {                                                                                \
        if (DEBUG>0) {                                                                  \
            printf("# %s:%d:%s(): " fmt, __FILE__,                                      \
                    __LINE__, __FUNCTION__, ##__VA_ARGS__);                             \
        }                                                                               \
    }                                                                                   \
    while(0)

// more verbose debugging
#define debug2(fmt, ...)                                                                \
    do {                                                                                \
        if (DEBUG>1) {                                                                  \
            printf("# %s:%d:%s(): " fmt, __FILE__,                                      \
                    __LINE__, __FUNCTION__, ##__VA_ARGS__);                             \
        }                                                                               \
    }                                                                                   \
    while(0)

#define debug_int(var)                                                                  \
    do {                                                                                \
        if (DEBUG>0) {                                                                  \
            printf("# %s:%d:%s(): int %s := %d\n" , __FILE__,                           \
                    __LINE__, __FUNCTION__, #var, var);                                 \
        }                                                                               \
    }                                                                                   \
    while(0)

#define debug_hex(var)                                                                  \
    do {                                                                                \
        if (DEBUG>0) {                                                                  \
            printf("# %s:%d:%s(): hex %s := 0x%x\n" , __FILE__,                         \
                    __LINE__, __FUNCTION__, #var, var);                                 \
        }                                                                               \
    }                                                                                   \
    while(0)
