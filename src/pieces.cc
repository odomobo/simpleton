#include "defs.h"

// a table of all the information of how to display and move the pieces
const struct Piece_Info piece_table[7] = {
    {   // space
        /*.display = */".?",
        /*.move_direction_count = */0,
        /*.move_directions = */{},
        /*.is_sliding = */false
    },
    {   // pawn; moves are generated using separate logic
        /*.display = */"Pp",
        /*.move_direction_count = */0,
        /*.move_directions = */{},
        /*.is_sliding = */false
    },
    {   // knight
        /*.display = */"Nn",
        /*.move_direction_count = */8,
        /*.move_directions = */{coord_get(-2, -1), coord_get(-2, 1), coord_get(-1, -2), coord_get(-1, 2), coord_get(1, -2), coord_get(1, 2), coord_get(2, -1), coord_get(2, 1)},
        /*.is_sliding = */false
    },
    {   // bishop
        /*.display = */"Bb",
        /*.move_direction_count = */4,
        /*.move_directions = */{coord_get(-1, -1), coord_get(-1, 1), coord_get(1, -1), coord_get(1, 1)},
        /*.is_sliding = */true
    },
    {   // rook
        /*.display = */"Rr",
        /*.move_direction_count = */4,
        /*.move_directions = */{coord_get(-1, 0), coord_get(0, -1), coord_get(0, 1), coord_get(1, 0)},
        /*.is_sliding = */true
    },
    {   // queen
        /*.display = */"Qq",
        /*.move_direction_count = */8,
        /*.move_directions = */{coord_get(-1, -1), coord_get(-1, 0), coord_get(-1, 1), coord_get(0, -1), coord_get(0, 1), coord_get(1, -1), coord_get(1, 0), coord_get(1, 1)},
        /*.is_sliding = */true
    },
    {   // king
        /*.display = */"Kk",
        /*.move_direction_count = */8,
        /*.move_directions = */{coord_get(-1, -1), coord_get(-1, 0), coord_get(-1, 1), coord_get(0, -1), coord_get(0, 1), coord_get(1, -1), coord_get(1, 0), coord_get(1, 1)},
        /*.is_sliding = */false
    }
};

Piece piece_from_display(char display) {
    for (Piece_Type piece = 0; piece < 7; piece++) {
        for (int color_index = 0; color_index < 2; color_index++) {
            if (piece_table[piece].display[color_index] == display) {
                return (piece|(color_index * COLOR_MASK));
            }
        }
    }

    debug("Not found: %d\n", display);
    return -1;
}