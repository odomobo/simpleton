#include <string.h>
#include <stdlib.h>
#include "defs.h"

void context_init() {
    // the stack must be initialized before initializing the board
    for (int i = 0; i < CONFIG_STACK_SIZE; i++) {
        Move_Vector * v = &c.stack[i].moves;
        v->init(16);
        c.stack[i].is_history = false;
    }
    c.stack_depth = 0;
    // The history stack doesn't technically need its move vectors, but we'll initialize them so we don't break anything
    // that assumes all stack frames have move vectors.
    for (int i = 0; i < CONFIG_HISTORY_SIZE; i++) {
        Move_Vector * v = &c.history[i].moves;
        v->init(0);
    }
    c.history_size = 0;
    c.king_position_cache[0] = COORD_INVALID;
    c.king_position_cache[1] = COORD_INVALID;
    // optimization initialization must be done before initializing the board
    optimizations_init();
    evaluation_init();
    board_init();
}

void context_destroy() {
    for (int i = 0; i < CONFIG_STACK_SIZE; i++) {
        Move_Vector * v = &c.stack[i].moves;
        v->destroy();
    }
}

void context_clear_state() {
    pv_table_clear();
    for (int i = 0; i < CONFIG_STACK_SIZE; i++) {
        memset(&c.stack[i].killer_moves, 0, sizeof(c.stack[i].killer_moves));
    }
}

// Performs a move, then:
// Adds another frame to the stack, and sets it with the data from the previous frame.
// Inverts the side to move from the previous frame.
// Checks castling status.
// Sets en passant, if applicable
// Clears move list.
void context_perform_move_and_push_stack(Move * m) {
    move_perform(m);
    Stack_Frame * so = stack_frame_get();
    c.stack_depth++;
    Stack_Frame * sn = stack_frame_get();

    // clear move list
    sn->moves.clear();

    // copy castling rights
    memcpy(&sn->can_castle, &so->can_castle, sizeof(bool[4]));

    // check castling rules for BOTH colors
    for (int i = 0; i < 2; i++) {
        int home_rank;
        Color color_;
        if (i == 0) {
            home_rank = 0;
            color_ = WHITE;
        } else {
            home_rank = 7;
            color_ = BLACK;
        }
        // check kingside castling first
        if (castling_check_kingside(sn, color_)) {
            if (piece_get(home_rank, 7) != (ROOK|color_) || piece_get(home_rank, 4) != (KING|color_)) {
                castling_set_kingside(sn, color_, false);
            }
        }
        // then check queenside castling
        if (castling_check_queenside(sn, color_)) {
            if (piece_get(home_rank, 0) != (ROOK|color_) || piece_get(home_rank, 4) != (KING|color_)) {
                castling_set_queenside(sn, color_, false);
            }
        }
    }

    Color old_color = c.to_move;
    // checks en passant
    if ((*m)[0].parity == (PAWN|old_color) && abs((*m)[1].coord - (*m)[0].coord) == 0x20) {
        sn->en_passant = (*m)[0].coord + ((*m)[1].coord - (*m)[0].coord) / 2;
    } else {
        sn->en_passant = COORD_INVALID;
    }

    // update whose turn it is to move
    c.to_move = color_other(c.to_move);
    // update the total halfmoves
    c.total_halfmoves++;

    // reset counter for the 50 move rule
    if (move_is_capture(m) || piece_get_type((*m)[0].parity) == PAWN) {
        sn->halfmoves_since_capture = 0;
    } else {
        sn->halfmoves_since_capture = so->halfmoves_since_capture + 1;
    }

    // it's important that these are done after the stack is all set up
    sn->in_check = board_is_king_attacked(c.to_move);
    sn->zh = zobrist_hashing_generate_from_board();
    history_table_add(sn);
}

void test_push_stack() {
    context_init();
    board_set_from_fen("r3k2r/3p4/8/8/8/8/3P4/R3K2R w KQkq - 0 1");
    mu_assert(c.total_halfmoves == 0);

    Move m1;
    generate_moves(false, false);
    board_find_move_from_string(&m1, "e1e2", false);
    context_perform_move_and_push_stack(&m1);
    mu_assert(c.stack_depth == 1);
    Stack_Frame * s = stack_frame_get();
    mu_assert(c.to_move == BLACK);
    mu_assert(c.total_halfmoves == 1);
    bool castle_kq[4] = {false, false, true, true};
    mu_assert(memcmp(&s->can_castle[0], castle_kq, sizeof(castle_kq)) == 0);
    context_perform_move_and_pop_stack(&m1);
    mu_assert(c.stack_depth == 0);
    mu_assert(c.total_halfmoves == 0);

    Move menpassantwhite;
    generate_moves(false, false);
    board_find_move_from_string(&menpassantwhite, "d2d4", false);
    context_perform_move_and_push_stack(&menpassantwhite);
    s = stack_frame_get();
    mu_assert(s->en_passant == 0x23);
    context_perform_move_and_pop_stack(&menpassantwhite);

    Move mempty = MOVE_EMPTY;
    context_perform_move_and_push_stack(&mempty);
    bool castle_KQkq[4] = {true, true, true, true};
    mu_assert(memcmp(&s->can_castle[0], castle_KQkq, sizeof(castle_KQkq)) == 0);

    Move menpassant;
    generate_moves(false, false);
    board_find_move_from_string(&menpassant, "d7d5", false);
    context_perform_move_and_push_stack(&menpassant);
    s = stack_frame_get();
    mu_assert(s->en_passant == 0x53);
    context_perform_move_and_pop_stack(&menpassant);

    Move m2;
    generate_moves(false, false);
    board_find_move_from_string(&m2, "h8g8", false);
    context_perform_move_and_push_stack(&m2);
    mu_assert(c.stack_depth == 2);
    s = stack_frame_get();
    mu_assert(c.to_move == WHITE);
    bool castle_KQq[4] = {true, true, false, true};
    mu_assert(memcmp(&s->can_castle[0], castle_KQq, sizeof(castle_KQq)) == 0);

    Move m3 = {{{(ROOK|WHITE), coord_get(0, 0)}, {(ROOK|WHITE), coord_get(5, 0)}}};
    generate_moves(false, false);
    board_find_move_from_string(&m3, "a1a6", false);
    context_perform_move_and_push_stack(&m3);
    s = stack_frame_get();
    bool castle_Kq[4] = {true, false, false, true};
    mu_assert(memcmp(&s->can_castle[0], castle_Kq, sizeof(castle_Kq)) == 0);
    context_destroy();
}

void context_perform_move_and_pop_stack(Move * m) {
    assert(c.stack_depth > 0);
    Stack_Frame * s = stack_frame_get();
    history_table_remove(s);
    c.stack_depth--;
    c.to_move = color_other(c.to_move);
    // keep track of move number
    c.total_halfmoves--;
    move_perform(m);
}

void stack_frame_move(Stack_Frame * dst, Stack_Frame * src) {
    // remove the old one from the history table
    history_table_remove(src);
    // copy entire stack frame, but be sure to preserve the move vector (which has dynamically allocated data)
    Move_Vector v = dst->moves;
    memcpy(dst, src, sizeof(Stack_Frame));
    dst->moves = v;
    dst->moves.clear();
    // after moving, add the new one to the history table
    history_table_add(dst);
}