// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include "types.h"

const Coord COORD_INVALID = 0x88;
const Coord FILE_MASK = 0x0F;
const Coord RANK_MASK = 0xF0;

char *      coord_to_string(Coord co);
bool        coord_is_attacked(Coord co, Color color);

inline Coord coord_get(int rank, int file) {
    return 16*rank + file;
}

inline bool coord_is_valid(Coord co) {
    return !(co & COORD_INVALID);
}