// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include "types.h"

const Score CHECKMATE_VALUE     = -1000;
const Score DRAW_VALUE          = 0;
const Score MIN_SCORE           = -10000;
const Score MAX_SCORE           =  10000;
const Score INFINTESIMAL_SCORE  = 0.0001f;

struct Evaluation_Table_Entry {
    Score material_value;
    float average_number_of_moves;
};

extern const Evaluation_Table_Entry evaluation_table[7];

struct Evaluation_Info {
    Score white_evaluation;
    Score material_values[2]; // in pawns
    float piece_utilization_values[2]; // an averagely-utilized piece has a value of 1
    float king_utilization_values[2]; // this is separated from the other pieces, because the king gets negative utilization near the beginning of the game
};

void evaluation_init();
Score evaluation_evaluate_position(Evaluation_Info * info_out);
void evaluation_evaluate_position_repl(char const * arg);