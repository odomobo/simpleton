// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include "types.h"

const int MOVE_COMPONENTS_PER_MOVE = 4;

struct Move_Component {
    Piece parity;
    Coord coord;
};

struct Move {
    Move_Component _components[MOVE_COMPONENTS_PER_MOVE];

    Move_Component & operator[] (const int index);
};

const Move MOVE_EMPTY = {{{0}}};

// score is just for ordering purposes; not total move evaluation
struct Scored_Move : Move {
    Score score;
};

char *      move_to_string(Move * m, bool extended);
void        move_display(Move * m, bool extended);
void        move_perform(Move * m);
bool        moves_do_match(Move * m1, Move * m2);
bool        move_is_capture(Move * m);
Piece_Type  move_get_capture_victim(Move * m);
bool        move_is_pawn_promotion(Move * m);
Compact_Move move_to_compact_move(Move * m);
void        move_find_from_compact_move(Move * dst, Compact_Move query);

inline bool move_is_empty(Move * m) {
    return (*m)[0].parity == 0;
}