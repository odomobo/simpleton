// DO NOT INCLUDE DIRECTLY. Instead, include "defs.h" into your source files.
#pragma once
#include "types.h"

struct Move_Vector {
    int count;
    int _capacity;
    Scored_Move * _data;

    Scored_Move & operator[] (const int index) {
        return _data[index];
    }
    void init(int capacity);
    void destroy();
    Scored_Move * alloc_elem();
    void remove_last();
    void sort();
    void clear();
};