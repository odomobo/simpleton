#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

#ifdef _WIN32
#  include <Windows.h>
#else
#  include "sys/select.h"
#  include "unistd.h"
#  include "string.h"
#endif

#include "defs.h"

// private
bool        _interruption_interrupting;
int         _interruption_check_counter = 0;
long long   _interruption_stop_time;
int         _interruption_input_waiting();
bool        _interruption_out_of_time();
// end private

void interruption_init() {
    // output must remain unbuffered in the xboard protocol
    setbuf(stdout, NULL);
    setbuf(stderr, NULL);
    _interruption_interrupting = false;
    _interruption_stop_time = 0;
}

void interruption_clear_interrupt() {
    _interruption_interrupting = false;
    _interruption_stop_time = 0;
}

// http://home.arcor.de/dreamlike/chess/
// Note: unix code is untested
int _interruption_input_waiting() {
#ifndef _WIN32
    fd_set readfds;
    struct timeval tv;
    FD_ZERO(&readfds);
    FD_SET(fileno(stdin), &readfds);
    tv.tv_sec = 0; tv.tv_usec = 0;
    select(16, &readfds, 0, 0, &tv);

    return (FD_ISSET(fileno(stdin), &readfds));
#else
    static int init = 0, pipe;
    static HANDLE inh;
    DWORD dw;

    if (!init) {
        init = 1;
        inh = GetStdHandle(STD_INPUT_HANDLE);
        pipe = !GetConsoleMode(inh, &dw);
        if (!pipe) {
            SetConsoleMode(inh, dw & ~(ENABLE_MOUSE_INPUT|ENABLE_WINDOW_INPUT));
            FlushConsoleInputBuffer(inh);
        }
    }
    if (pipe) {
        if (!PeekNamedPipe(inh, NULL, 0, NULL, &dw, NULL))
            return 1;
        return dw;
    } else {
        GetNumberOfConsoleInputEvents(inh, &dw);
        return dw <= 1 ? 0 : dw;
    }
#endif
}

bool _interruption_out_of_time() {
    if (_interruption_stop_time == 0)
        return false;
    return utils_get_time_milliseconds() >= _interruption_stop_time;
}

bool interruption_should_interrupt() {
    if (_interruption_interrupting)
        return true;
    // Only do the expensive checks occasionally, since this function gets called repeatedly
    // inside of the alpha beta search.
    if (_interruption_check_counter++ % CONFIG_INTERRUPT_FREQUENCY != 0) {
        return false;
    }
    if (_interruption_input_waiting() || _interruption_out_of_time()) {
        _interruption_interrupting = true;
        _interruption_stop_time = 0;
        return true;
    } else {
        return false;
    }
}

void interruption_set_timer(float seconds) {
    int milliseconds = seconds * 1000;
    milliseconds -= CONFIG_INTERRUPT_TIMER_FUDGE_FACTOR * 1000;
    _interruption_stop_time = utils_get_time_milliseconds() + milliseconds;
}