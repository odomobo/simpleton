#pragma once
#include "types.h"

void        interruption_init();
void        interruption_clear_interrupt();
bool        interruption_should_interrupt();
void        interruption_set_timer(float seconds);