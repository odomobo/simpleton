#include "defs.h"

char * coord_to_string(Coord co) {
    char * ret = (char *)malloc(3);
    sprintf(ret, "%c%c", 'a' + (co & FILE_MASK), '1' + (co & RANK_MASK)/16);
    return ret;
}

// Check if a square is being attacked. The color is the defender's color.
bool coord_is_attacked(Coord co, Color color) {
    // try knight moves
    for (int i = 0; i < piece_table[KNIGHT].move_direction_count; i++) {
        Coord test = co + piece_table[KNIGHT].move_directions[i];
        // if it's an enemy knight
        if (coord_is_valid(test) && piece_get_by_coord(test) == (KNIGHT|color_other(color)))
            return true;
    }
    // try pawn moves
    int pawn_move_dir = color_other_pawn_direction(color);
    for (int file_offset = -1; file_offset <= 1; file_offset += 2) {
        // we're moving backwards compared to the pawn, so we need the negative pawn move direction
        Coord test = co + coord_get(-pawn_move_dir, file_offset);
        // if it's an enemy pawn
        if (coord_is_valid(test) && piece_get_by_coord(test) == (PAWN|color_other(color)))
            return true;
    }
    // try sliding moves
    for (int i = 0; i < piece_table[QUEEN].move_direction_count; i++) {
        for (int j = 1; ; j++) {
            Coord test = co + (piece_table[QUEEN].move_directions[i] * j);
            // ran off the edge
            if (!coord_is_valid(test)) {
                break;
            }
            Piece piece = piece_get_by_coord(test);
            if (piece == SPACE)
                continue;
            // an enemy piece
            if (piece_get_color(piece) == color_other(color)) {
                Piece_Type p = piece_get_type(piece);
                // non-sliding pieces don't count if we're more than 1 move away
                if (j > 1 && !(piece_table[p].is_sliding)) {
                    break;
                }
                if (piece_type_can_move_like(p, piece_table[QUEEN].move_directions[i]))
                    return true;
                // if the piece doesn't contain this particular move, then break
                break;
            }
            // by the process of elimination, this must be a friendly piece.
            break;
        }
    }
    // if none of the above found anything, then we must not be attacked
    return false;
}