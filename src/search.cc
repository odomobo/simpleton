#include <math.h>
#include <string.h>
#include "defs.h"

// private
static void _search_order_moves(Move * pv);
static Score _search_alpha_beta(int depth, Score alpha, Score beta);
// end private

// Sets the score on each move based on pv, killer heuristic, captures, etc.
// Sorts the moves based on this information.
// Moves must have already been generated.
static void _search_order_moves(Move * pv) {
    Stack_Frame * s = stack_frame_get();
    for (int i = 0; i < s->moves.count; i++) {
        Score value = 0;
        Move * m = &s->moves[i];
        if (moves_do_match(m, pv)) {
            value = 1900000;
        } else if (move_is_pawn_promotion(m)) {
            if (move_is_capture(m)) {
                Piece_Type victim = move_get_capture_victim(m);
                value = 1800000 + 1000*evaluation_table[victim].material_value;
            } else {
                value = 1800000;
            }
        } else if (move_is_capture(m)) {
            Piece_Type attacker = piece_get_type((*m)[0].parity);
            Piece_Type victim = move_get_capture_victim(m);
            value = 1700000;
            // most valuable victim
            value += 1000*evaluation_table[victim].material_value;
            // least valuable attacker
            value += 100*(10 - evaluation_table[attacker].material_value);
        } else {
            context_perform_move_and_push_stack(m);
            value = -evaluation_evaluate_position(NULL);
            context_perform_move_and_pop_stack(m);
        }
        // TODO: killer moves

        // store the move score
        s->moves[i].score = value;
    }

    s->moves.sort();
}

// performs quiescence search when depth is at 0 or lower
static Score _search_alpha_beta(int depth, Score alpha, Score beta) {
    // hard cutoff
    Stack_Frame * s = stack_frame_get();
    if (c.stack_depth >= CONFIG_STACK_SIZE-1) {
        if (s->in_check) {
            return CHECKMATE_VALUE;
        } else {
            return evaluation_evaluate_position(NULL);
        }
    }
    
    // Unlimited search extension for check.
    // But realistically, there are a limited chain of capturing out of check while putting the opponent into check.
    if (depth >= 0 && s->in_check) {
        depth++;
    }
    bool quiesce = depth <= 0;
    PV_Entry * e = pv_table_get_entry();
    
    // We can just return the pv table entry's score if it has proper depth
    if (e && e->depth >= depth)
        return e->score;
    
    Score max_so_far = MIN_SCORE;
    if (quiesce) {
        max_so_far = evaluation_evaluate_position(NULL);
        // see if we can break early
        alpha = fmaxf(alpha, max_so_far);
        if (beta <= alpha) {
            return max_so_far;
        }
    }
    generate_moves(false, quiesce);

    // get the actual move based on the compact move
    Move pv = MOVE_EMPTY;
    if (e)
        move_find_from_compact_move(&pv, e->best_move);

    Result result = result_check(quiesce);
    if (result != RESULT_NONE)
        return result_scores[result]; // return the score of this result; note that a checkmate will always be a positive value, which is what we want

    _search_order_moves(&pv);
    memset(&pv, 0, sizeof(pv)); // clear out the old pv
    for (int i = s->moves.count-1; i >= 0; i--) {
        Move * m = &s->moves[i];
        context_perform_move_and_push_stack(m);
        Score current_value = -_search_alpha_beta(depth-1, -beta, -alpha);
        // If we found a new pv, make sure to save it! We don't want to store it in the table until we are
        // done with all the branches, however.
        if (current_value > alpha)
            pv = *m;
        max_so_far = fmaxf(max_so_far, current_value);
        context_perform_move_and_pop_stack(m);

        // alpha beta stuff
        alpha = fmaxf(alpha, max_so_far);
        if (beta <= alpha) {
            break;
        }
        if (interruption_should_interrupt()) {
            memset(&pv, 0, sizeof(pv)); // don't store the pv!
            break;
        }
            
    }
    // cleanup!
    s->moves.clear();

    // we save fail-highs just like normal values
    if (!move_is_empty(&pv))
        pv_table_add_move(&pv, alpha, depth);

    return max_so_far;
}

// Note that the returned move can't be used anymore, after a Move_Vector::clear
// has been performed.
void search(int depth, Scored_Move * sm)  {
    // This has to be done before every new search because PV entries of a lower depth won't overwrite nodes
    // of a higher depth, regardless of whether or not it's the same position. If we didn't clear it, the
    // PV table would soon be filled with worthless positions and cease to be useful
    pv_table_clear();
    c.counter = 0;

    // this is so we can show the elapsed time in the post information
    long long start_time = utils_get_time_milliseconds();
    PV_Entry * e = NULL;
    for (int i = 1; i <= depth; i++) {
        _search_alpha_beta(i, MIN_SCORE, MAX_SCORE);
        e = pv_table_get_entry();
        // make sure we store the PV_Entry before breaking
        if (interruption_should_interrupt())
            break;
        if (e != NULL) {
            char * pv_string = pv_table_get_best_line(true);
            if (xboard_post) {
                int elapsed_time = utils_get_time_milliseconds() - start_time;
                printf("%d %d %d %d %s\n", i, (int)(e->score* 100), elapsed_time/10, c.counter, pv_string);
            } else {
                printf("# Depth: %d; Score: %.4f; PV: %s\n", i, e->score, pv_string);
            }
            
            free(pv_string);
            if (e->score == -CHECKMATE_VALUE || e->score == CHECKMATE_VALUE)
                break;
        } else {
            printf("# No moves\n");
            break;
        }
    }

    if (sm != NULL) {
        // generate moves so we can look up the actual move based on the compact move.
        generate_moves(false, false);
        move_find_from_compact_move(sm, e->best_move);
        sm->score = e->score;
    }
}

void search_repl(char const * depth_str) {
    int depth = atoi(depth_str);
    if (depth <= 0)
        depth = 9999;
    c.counter = 0;

    // we have to clear any lingering interrupt
    interruption_clear_interrupt();
    long long t1 = utils_get_time_milliseconds();
    search(depth, NULL);
    long long t2 = utils_get_time_milliseconds();
    float time = (t2 - t1) / 1000.0f;
    utils_display_performance(time);
}